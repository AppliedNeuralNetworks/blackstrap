#pragma once

#include <czmq.h>

#include "pathname.h"
#include "blackstrap-conf.h"
#include "blackstrap-log.h"
#include "lockfile.h"
#include "blackstrap-stringify.h"
#include "blackstrap-protocol.h"
#include "blackstrap-i2cd-client.h"
#include "blackstrap-i2cd-field.h"
#include "blackstrap-gpio.h"
#include "blackstrap-misc.h"
#include "blackstrap-pathlist.h"

#define ZFRAME_FINAL (0)

//---------------------------------------------- Initialization and Termination

/** Initialize the Blackstrap Library
 *
 * Applications written using the Blackstrap library should call this
 * function as early as possible during the initialization phase.  The
 * logging system will be initialized, and the application configuration
 * files will be loaded.  A special case is when a specific configuration
 * specified on the command line should be loaded. One solution is to parse
 * options first, then call blackstrap_init(). However, if you require a
 * special logging configuration, a second sequence is more optimal:
 * initialize blackstrap with blackstrap_init() to establish logging, then
 * parse any options, and finally load the specific configuration file
 * separately by calling blackstrap_appconf_load_specific_conf(). 
 * FIXME: Idempotence?
 */
void blackstrap_init(void);

/** Clean Up Data Structures used by Blackstrap at Program Termination
 *
 * Unloads application configuration data, and shuts down the logging system.
 * FIXME: Idempotence?
 */
void
blackstrap_cleanup(void);

//--------------------------------------------------- Application Configuration

/* Blackstrap applications are free to load configurations from files and
 * use them in any way. However, a central repository for the
 * application's global configuration is desirable. This may include
 * compiled-in defaults, application configuration files from various
 * places in the filesystem, parsed command-line options, and other types
 * of data, and may be loaded automatically or manually at run-time. To
 * facilitate this, the Blackstrap library keeps a stack of configuration
 * pointers in an array. When a configuration value is requested, the
 * individual configurations are searched from the first position forward,
 * and the first match is returned. This allows higher priority data such
 * as command line options to override configuration files, which in turn
 * override compiled-in defaults.
 *
 * When Blackstrap is initialized with blackstrap_init(), 
 * blackstrap_appconf_init() is called, and available configurations are
 * loaded automatically (see below). Calling blackstrap_cleanup() will 
 * call blackstrap_appconf_cleanup(), which destroys any remaining 
 * structures in the stack.
 */
typedef enum blackstrap_appconf_level_e {
    BLACKSTRAP_APPCONF_RUNTIME,    // highest level overrides at runtime
    BLACKSTRAP_APPCONF_PATHINFO,   // computed info about app's name & path
    BLACKSTRAP_APPCONF_OPTIONS,    // command line options
    BLACKSTRAP_APPCONF_SPECIFIC,   // config specified on command line with -c
    BLACKSTRAP_APPCONF_PACKAGE,    // config file from package directory
    BLACKSTRAP_APPCONF_USER,       // config file from user's home directory
    BLACKSTRAP_APPCONF_SYSTEM,     // config file from system (/etc/...)
    BLACKSTRAP_APPCONF_DEFAULTS,   // fallback compiled-in defaults

    BLACKSTRAP_APPCONF_MAX
} blackstrap_appconf_level;

/** Get a Configuration from the Application Configuration Container
 *
 * level: integer < BLACKSTRAP_APPCONF_MAX, or blackstrap_appconf_level
 *
 * Returns: zconfig_t * on success, NULL if config level wasn't set
 */
zconfig_t *
blackstrap_appconf_level_get(blackstrap_appconf_level level);

/** Store a Configuration into the Application Configuration Container
 *
 * conf: a zconfig_t *
 * level: (see above)
 */
void
blackstrap_appconf_level_set(zconfig_t *conf, blackstrap_appconf_level level);

/** Retrieve a Value from the Application Configuration Stack
 *
 * The configuration stack is traversed from the first entry (highest
 * level of overriding) to the last entry, returning the value of
 * the first matching entry, if found, else the default is returned.
 *
 * path: the path of the value in a configuration, such as "network/address"
 *
 * Returns: const char * if a match is found, otherwise default_value
 */
const char *
blackstrap_appconf_resolve(const char *path, const char *default_value);

/** Load a Configuration Specified on the Command Line
 *
 * When blackstrap_init() is called, configurations are loaded. If
 * "configuration/filename" is resolvable, that configuration is
 * loaded into the appconf level for the SPECIFIC configuration.
 * Generally, this field would be populated by an option parser
 * sometime before blackstrap_init() is called. However, if you
 * desire special logging during command line parsing, you
 * may choose to call blackstrap_init() first. Because of this,
 * no specific configuration file name will be found, and the
 * specific configuration will not be loaded. In this case,
 * call this function after you parse command line options
 * to (possibly) load the specific configuration.
 */
void
blackstrap_appconf_load_specific_conf(void);

