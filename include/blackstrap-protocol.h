#pragma once

#include <time.h>
#include <czmq.h>

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ send frame ] 
/**
 * Protocol: Send a status frame to the client
 * To notify clients of an error from the remote endpoint, messages
 * to the client generally carry a signed integer status as their
 * first frame.
 * sock: a connected blackstrap endpoint
 * status: an arbitrary unsigned interger
 * flags: ZFRAME_MORE if more frames will follow, or 0 to indicate the
 *        final frame of a message
 */
int blackstrap_send_status(zsock_t *sock, int status, int flags);

/**
 * Protocol: Send a status frame to the client indicating success
 * Convenience function to send a successful status frame (0).
 * sock, flags: (see above)
 */
int blackstrap_send_status_success(zsock_t *sock, int flags);

/**
 * Protocol: Send a status frame to the client indicating failure
 * Convenience function to send an unsuccessful status frame (-1).
 * Note that it is presumed that a failure means that no more frames
 * will follow.
 * sock: (see above)
 */
int blackstrap_send_status_failure(zsock_t *sock);

int blackstrap_send_int64(zsock_t *sock, int64_t value, int flags);

int blackstrap_send_action(zsock_t *sock, int action, int flags);
int blackstrap_send_bytes(zsock_t *sock, uint8_t *data, size_t length,
	int flags);
int blackstrap_send_byte(zsock_t *sock, uint8_t data, int flags);
int blackstrap_send_time(zsock_t *sock, struct timespec *tp, int flags);


int64_t blackstrap_msg_shift_int64(zmsg_t *msg);
uint8_t blackstrap_msg_shift_byte(zmsg_t *msg);
size_t blackstrap_msg_shift_bytes(zmsg_t *msg, uint8_t *buf);
size_t blackstrap_msg_bytes_get_len(zmsg_t *msg);
int blackstrap_msg_shift_time(zmsg_t *msg, struct timespec *tp);
int blackstrap_msg_shift_status(zmsg_t *msg);
int blackstrap_msg_shift_action(zmsg_t *msg);
void blackstrap_msg_shift(zmsg_t *msg);

int blackstrap_recv_status(zsock_t *sock);
