#pragma once

#ifndef LOCKFILE_PATH
#    define LOCKFILE_PATH    "/var/lock/"
#endif

#ifndef LOCKFILE_SUFFIX
#    define LOCKFILE_SUFFIX  ".lock"
#endif

int lockfile_acquire(void);
int lockfile_release(void);

int lockfile_kill_process(void);
