#pragma once

#include <argp.h>

/** Initialize logging
 *
 * Performs an explicit openlog() with facility LOG_DAEMON. Logs to
 * syslog, falling back to console (if there's an error logging to syslog). 
 * Logs to stderr additionally.
 */
void
blackstrap_log_init(void);

/** Cleanup logging facility
 *
 * Shuts down the logging facility
 */
void
blackstrap_log_cleanup(void);

void
blackstrap_log_error(const char *format, ...);

void
blackstrap_do_or_die(int status, const char *format, ...);

void
blackstrap_log_error_exit(const char *format, ...);

void
blackstrap_log_error_abort(const char *format, ...);

void
blackstrap_log_warning(const char *format, ...);

void
blackstrap_log_notice(const char *format, ...);

void
blackstrap_log_info(const char *format, ...);

void
blackstrap_log_debug(const char *format, ...);

void
blackstrap_vlog(int priority, const char *format, va_list argptr);

void
blackstrap_log_uchar_msg_value(const char *message, unsigned char value);

