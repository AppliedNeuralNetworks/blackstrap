/*
 * To use pin 4 as an input pin:
 *     blackstrap_gpio_export(4);
 *  
 *     int val;
 *     blackstrap_gpio_read_value(4, &val);
 */

// NOTE:
// 
// GPIO group ownership/modes aren't being set correctly as of 2016-07-06.
// The contents of /lib/udev/rules.d/60-python-pifacecommon.rules need to
// be replaced with:
// KERNEL=="spidev*", GROUP="spi", MODE="0660"
// SUBSYSTEM=="gpio*", PROGRAM="/bin/sh -c 'chown -fR root:gpio
//   /sys/class/gpio /sys/devices/virtual/gpio /sys/devices/soc/*.gpio/gpio
//   /sys/devices/platform/soc/*.gpio/gpio; chmod -fR 770 /sys/class/gpio
//   /sys/devices/virtual/gpio /sys/devices/soc/*.gpio/gpio
//   /sys/devices/platform/soc/*.gpio/gpio'"
// 
// (The SUBSYSTEM definition should be together on one line.)

#ifndef BLACKSTRAP_GPIO_FILENAME_MAX_LEN
#define BLACKSTRAP_GPIO_FILENAME_MAX_LEN (64)
#endif

#ifndef BLACKSTRAP_FILE_VALUE_MAX_LEN
#define BLACKSTRAP_FILE_VALUE_MAX_LEN (32)
#endif

#ifndef BLACKSTRAP_GPIO_EXPORT_MAX_WAIT_MS
#define BLACKSTRAP_GPIO_EXPORT_MAX_WAIT_MS (1000)
#endif

#define BLACKSTRAP_GPIO_DIRECTION_IN "in"
#define BLACKSTRAP_GPIO_DIRECTION_OUT "out"

#define BLACKSTRAP_GPIO_EDGE_NONE "none"
#define BLACKSTRAP_GPIO_EDGE_RISING "rising"
#define BLACKSTRAP_GPIO_EDGE_FALLING "falling"
#define BLACKSTRAP_GPIO_EDGE_BOTH "both"

#define BLACKSTRAP_GPIO_ACTIVE_LOW_DISABLE (0)
#define BLACKSTRAP_GPIO_ACTIVE_LOW_ENABLE (1)


int blackstrap_gpio_export(int pin);
int blackstrap_gpio_unexport(int pin);
int blackstrap_gpio_exported(int pin);

ssize_t blackstrap_gpio_get_edge(int pin, char *edge);
int blackstrap_gpio_set_edge(int pin, const char *edge);

ssize_t blackstrap_gpio_get_direction(int pin, char *direction);
int blackstrap_gpio_set_direction(int pin, const char *direction);

ssize_t blackstrap_gpio_get_active_low(int pin, int *active_low);
int blackstrap_gpio_set_active_low(int pin, int active_low);

int blackstrap_gpio_get_value(int pin, int *value);
ssize_t blackstrap_gpio_set_value(int pin, int value);

