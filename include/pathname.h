#pragma once

char *dir_name(const char *path);
char *parent_dir_name(const char *path);
char *parent_parent_dir_name(const char *path);

char *base_name(const char *path);
char *parent_base_name(const char *path);
char *parent_parent_base_name(const char *path);

int   base_name_is(const char *path, const char *str);
int   parent_base_name_is(const char *path, const char *str);
int   parent_parent_base_name_is(const char *path, const char *str);

int   dir_name_is(const char *path, const char *str);
int   parent_dir_name_is(const char *path, const char *str);
int   parent_parent_dir_name_is(const char *path, const char *str);


