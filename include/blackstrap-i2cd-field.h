#pragma once

#include <czmq.h>

/** Load device configuration file
 *
 * Device configuration files describe the fields and registers of a chip
 * on the I2C bus. For interoperability, these files are in YAML format.
 * For ease of access, they are loaded into a zconfig_t structure. Unless
 * the filename given is an absolute pathname, loading is attempted first
 * from the "devices" subdirectory of the project dir, then from the same
 * subdirectory of the system configuration directory. In other words,
 * a load of "mydevice" will try:
 *      .../{project-dir}/conf/devices/mydevice.yaml
 *      and then
 *      /etc/blackstrap/devices/mydevice.yaml
 * The first extant path will be loaded. The project directory location
 * is not attempted if there is no project dir or the paths have not been
 * initialized in the configuration with blackstrap_init(). When finished,
 * destroy with i2cd_field_unload(&conf);
 *
 * device_name: name of device, such as "lsm303dlhc" (without path or 
 * extension), or absolute path to file
 *
 * Returns: zconfig_t * on success, -1 on failure
 */
zconfig_t *
blackstrap_i2cd_field_load(const char *device_name);

/** Destroy a loaded device configuration data structure
 *
 * Tears down the zconfig_t data structure, and nullifies the pointer.
 *
 * conf: pointer to pointer to a zconfig_t
 */
void
blackstrap_i2cd_field_unload(zconfig_t **conf);

/** Get the value of a field from a remote I2C device
 *
 * sock: a ZMQ socket connected to i2cd (see i2cd_connect())
 * conf: a device configuration loaded with i2cd_field_conf_load()
 * bus: the I2C bus on the remote host to which the device is connected
 * field_name: a field name from the device configuration
 * timestamp: a pointer to a struct timespec, or NULL if time isn't needed
 *
 * Returns: the value of the field on success, -1 on failure
 */
int
blackstrap_i2cd_field_get(zsock_t *sock, zconfig_t *conf, unsigned char bus,
       	const char *field_name, struct timespec *timestamp);

/** Get a description of the value of a field from a remote I2C device
 *
 * sock, conf, bus, field_name, timestamp: (see above)
 *
 * Returns an allocated string. Free with free().
 */
char *
blackstrap_i2cd_field_str_get(zsock_t *sock, zconfig_t *conf,
       	unsigned char bus, const char *field_name,
       	struct timespec *timestamp);

/** Set the value of a field on a remote I2C device
 *
 * sock, conf, bus, field_name: (see above)
 * value: desired value for the field
 *
 * Returns: 0 on success, -1 on failure
 */
int
blackstrap_i2cd_field_set(zsock_t *sock, zconfig_t *conf, unsigned char bus,
       	const char *field_name, unsigned char value);

/** Set the value of a field on a remote I2C device by description
 *
 * sock, conf, bus, field_name: (see above)
 * descr: a description from the descr block of a field in the configuration
 *
 * Returns: 0 on success, -1 on failure
 */
int
blackstrap_i2cd_field_str_set(zsock_t *sock, zconfig_t *conf,
       	unsigned char bus, const char *field_name, const char *descr);

/** Get a range of int16_t values from a series of registers on a device
 *
 * buf: buffer for int16_t values (must be NULL)
 * sock, conf, bus: (see above)
 * range_name: name of int16 range from device configuration
 * 
 * Returns: 0 on success, -1 on failure
 */
long
blackstrap_i2cd_field_get_range_int16(int16_t **buf, zsock_t *sock,
       	zconfig_t *conf, unsigned char bus, const char *int_range_name);

