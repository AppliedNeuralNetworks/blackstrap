#pragma once

/** Find the Last Occurence of a Sub-string Within a String
 *
 * haystack: the string in which to search
 * needle: the string for which to search
 *
 * Returns: char * if a match was found, NULL if no match
 */
char *
strrstr(const char *haystack, const char *needle);

/** Open a Text File and Retrieve a Long from the First 16 Characters
 *
 * value: pointer to a long type to contain the retreived data
 * filename: path of file to open
 *
 * Returns: 0 on success, -1 on error
 */
int
get_long_from_file(long *value, const char *filename);

/** Format a timespec into a Character String with Nanosecond Precision
 *
 * Output is similar to ISO8601, without the intervening "T", and
 * with decimal seconds (nanosecond precision, though the underlying
 * operating system may not provide the full accuracy). NOT thread-safe.
 *
 * timestamp: timespec to be formatted, in GMT time
 *
 * Returns: pointer to static string containing a formatted localized time
 */
char *
format_time_8601plus(struct timespec *timestamp);

