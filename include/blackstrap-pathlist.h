#pragma once

typedef struct _zlist_t blackstrap_pathlist_t;

blackstrap_pathlist_t *
blackstrap_pathlist_new(void);

void
blackstrap_pathlist_destroy(blackstrap_pathlist_t **pathlist);

int
blackstrap_pathlist_append(blackstrap_pathlist_t *pathlist,
       	const char *format, ...);

int
blackstrap_pathlist_prepend(blackstrap_pathlist_t *pathlist,
       	const char *format, ...);

const char *
blackstrap_pathlist_first_existing(blackstrap_pathlist_t *pathlist);

