#pragma once

/** Stringify an unsigned char into a binary representation.
 * 
 * value: value to be converted into a string
 *
 * Returns an allocated string. Free with free().
 */
char *
blackstrap_stringify_uchar_bin(unsigned char value);

/** Stringify an unsigned char
 *
 * Build a string with decimal, hex, and binary representations of a value.
 *
 * value: (see above)
 *
 * Returns an allocated string. Free with free().
 */
char *
blackstrap_stringify_uchar(unsigned char value);

/** Stringify an unsigned char with a message prefix
 *
 * message: string with the message prefix
 * value: (see above)
 *
 * Returns an allocated string. Free with free().
 */
char *
blackstrap_stringify_msg_uchar(const char *message, unsigned char value);

