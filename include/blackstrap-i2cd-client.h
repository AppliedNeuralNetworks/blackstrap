#pragma once

#include <time.h>

enum i2cd_action {
    I2CD_ACTION_NOOP = 0,

    I2CD_ACTION_READ,
    I2CD_ACTION_READ_BYTE,
    I2CD_ACTION_WRITE,
    I2CD_ACTION_WRITE_BYTE,

    I2CD_ACTION__MAX_
};  

/** Connect to i2cd
 *
 * Disconnect with i2cd_disconnect() or blackstrap_disconnect()
 *
 * endpoint: a ZMQ-style endpoint, such as "tcp://127.0.0.1:9000"
 *
 * Returns: the ZMQ socket on success, NULL on error
 */
zsock_t *
blackstrap_i2cd_connect(const char *endpoint);

/** Do nothing
 *
 * Performs a NOOP action on the server, which will only return a
 * successful status.
 *
 * sock: a zsocket connected to the i2cd server
 * timestamp: pointer to struct timespec, or NULL if time is not desired
 *
 * Returns: the status from the server -- always SUCCESS (0)
 */
int
blackstrap_i2cd_noop(zsock_t *sock, struct timespec *timestamp);

/** Read a series of bytes from an I2C peripheral.
 *
 *  sock, timestamp: (see above)
 *  bus: the I2C bus device number, as in /dev/i2c-1. On a Raspberry Pi
 *       Model A, bus 0 is on the headers. In a Model B, it's bus 1.
 *  address: the address of the peripheral on the I2C bus. Note that
 *       the address given in many datasheets gives the address shifted
 *       one bit to the left. This is because the I2C device receives
 *       8 bits instead of 7 and expects the LSB to denote a read or write
 *       operation. Linux thinks of these addresses with their normal
 *       7-bit representation. For example, the datasheet for the 
 *       LSM303DLHC Accelerometer/Magnetometer describes its address as
 *       0x32. Meanwhile, a search for the device on the bus with
 *       "i2cdetect -y 1" shows the device at address 0x19. To make
 *       this clear in your code, you might specify the address of this
 *       device explicitely as "0x32 >> 1"
 *  command: I2C devices interpret this to mean different things. Some
 *       devices may interpret it a directive. Most will use the value
 *       to set a pointer to a register from which you may read or write.
 *  buf: pointer to an array of uint8_t, length or longer
 *  length: the number of bytes to read from the device.
 *
 *  Returns 0 on success, -1 on failure.
 */
int
blackstrap_i2cd_read(zsock_t *sock, uint8_t bus, uint8_t address,
	uint8_t command, uint8_t *buf, uint8_t length,
	struct timespec *timestamp);

/** Read a single byte from an I2C peripheral.
 *
 *  sock, bus, address, command, timestamp: (see above)
 *
 *  Returns: positive on success (bottom 8 bits are the data), -1 on failure
 */
uint8_t
blackstrap_i2cd_read_byte(zsock_t *sock, uint8_t bus, uint8_t address,
       	uint8_t command, struct timespec *timestamp);

/** Write a single byte to an I2C peripheral.
 *
 *  sock, bus, address, command: (see above)
 *
 *  Returns: 0 on success, -1 on failure
 */
int
blackstrap_i2cd_write_byte(zsock_t *sock, uint8_t bus, uint8_t address,
       	uint8_t command, uint8_t value);

/** Write a series of bytes to an I2C peripheral.
 *
 *  sock, bus, address, command: (see above)
 *  data: array of bytes
 *  length: number of bytes to write
 *
 *  Returns: 0 on success, -1 on failure
 */
int
blackstrap_i2cd_write(zsock_t *sock, uint8_t bus, uint8_t address,
	uint8_t command, uint8_t *data, uint8_t length);

