#pragma once

#include <czmq.h>

/** Load a Configuration from a ZPL File
 *
 * Loads a configuration from a ZPL file. Does not affect the
 * configuration stack.
 *
 * filename: a path to a filename containing valid ZPL data
 *
 * Returns: zconfig_t * on success, NULL on failure
 */
zconfig_t *
blackstrap_conf_load(const char *filename);

/** Load a Configuration from a YAML File
 *
 * Loads a configuration from a YAML file. Does not affect the
 * configuration stack.
 *
 * filename: a path to a filename containing valid YAML data
 *
 * Returns: zconfig_t * on success, NULL on failure
 */
zconfig_t *
blackstrap_conf_load_yaml(const char *file_name);

/** Resolve a Configuration Path to a long int
 *
 * dest: pointer to long integer
 * conf: zconfig_t *
 * path: zconfig path, such as "network/bind"
 *
 * Returns: 0 on success, -1 if no match, -ERANGE on conversion failure
 */
int
blackstrap_conf_resolve_long(long *dest, zconfig_t *conf, const char *path);

/** Resolve a Configuration Path to a long int, with Default Value Fallback
 *
 * dest, conf, path: (see above)
 * default_value: no-match fallback default
 *
 * Returns: 0 on success, -ERANGE on conversion failure
 */
int
blackstrap_conf_resolve_long_default(long *dest, zconfig_t *conf,
       	const char *path, long default_value);

