# What is Blackstrap?

Blackstrap is a small suite of very small programs that give your
programs simple, fast access to peripherals on your computer's
[I²C](http://en.wikipedia.org/wiki/I%C2%B2C) bus. If you have [Raspberry
Pi](http://www.raspberrypi.org/), a [Beaglebone
Black](http://beagleboard.org/black), or any other computer running
Linux, and you want to be able to talk to breakout boards like the many
excellent [Adafruit](http://www.adafruit.com/) boards, or perhaps a
[MiniIMU](http://www.pololu.com/product/2468), Blackstrap will hook you
up, from anywhere on your network, or anywhere on the internet.

## How does Blackstrap work?

Blackstrap uses a messaging protocol to communicate. If you run i2cd,
you can trivially write to and read from peripherals on the i²C bus just
by sending messages to i2cd. A client API is provided for both C and
Python which abstracts this lightweight protocol. Further more, with
Blackstrap, it's easy to write new components, since the networking
parts are already taken care of. Run any of the components on any hosts.
Choose your own network architecture. Extend the capabilities. With
Blackstrap handling the hardware, you can turn your attention to the fun
stuff: playing with sensors, servos, buttons, and more!

## What's under the hood?

Blackstrap is proudly built on the [0MQ](http://zeromq.org/) messaging
system. It's elegant, light, and incredibly speedy. It's also trivial to
develop programs that can talk 0MQ. Blackstrap's core programs are
written in C, but can be accessed from C, C++, Python, Julia, or
anything that can talk 0MQ. Since 0MQ is network savvy, you can talk
across your network and send and receive data to and from the goodies on
your RPi. Examples are provided so you can get started right away.

## What's Blackstrap's status?

Blackstrap is a new project, with all the caveats and disclaimers. We're
using it in our own projects, so we'll be testing new components as we
go. I2cd is ready for use, and the results so far are amazing. Next
we'll be adding support for the serial bus (to use, for example, the
[Adafruit Ultimate GPS Breakout](http://www.adafruit.com/products/746)),
with SPI and perhaps other busses to follow -- and perhaps other types
of non-busses, like GPIO.

With I²C accessible from anywhere on your network, you can tap into
loads of sensors and controllers, and network hosts full of devices
together to build larger systems. We've mentioned the [Adafruit
9DOF](http://www.adafruit.com/products/1714) and MiniMU boards, but just
imagine 16-channel, 12-bit PWM (PCA9685) servo controllers, Analog to
Digital conversion using the ADS1115, 8-channel capacitive key touch
using the CAP1188, thermometers, barometers, motion sensors, and more!

## What's in a name?

Blackstrap follows the delicious culinary motif of the Raspberry Pi:
blackstrap molasses is a common ingredient in baking. Then, too, there
is the handsome black ribbon cable that connects your Raspberry Pi to a
prototyping board if you have purchased Adafruit's fine
[Pi-Cobbler](http://www.adafruit.com/products/914) or
[T-Cobbler](http://www.adafruit.com/products/1754). That's the "black
strap" that we bridge with this software. Molasses is a commonly used
analogy for slowness, and reminds us that the RPi isn't a super-fast
computer. Sure, it's small and light and brimming with a certain happy
efficiency (3.5W TDP!), but we want interfacing software that's small
and fast, so we can make the most of the hardware, leaving us enough CPU
cycles to run our workload that uses the data for whatever we'd like to
do with it. And because the data is network-accessible, you can easily
capture the data from larger machines that can crunch the numbers with
ease.

Let us know how you'd like to use Blackstrap. We welcome participation.
Come join us! Contact us at blackstrap (at) appliedneural (dot) net.

## How Do I Download and Install Blackstrap?

See our [installation guide](https://project.appliedneural.net/blackstrap/wiki/BlackstrapInstallation).
