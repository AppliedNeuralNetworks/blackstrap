
#include <stdlib.h>
#include <error.h>
#include <argp.h>

#include "blackstrap.h"
#include "options.h"

static zconfig_t *options_conf;

const char *argp_program_version = "blackstrap i2cd 0.1";
const char *argp_program_bug_address = "<blackstrap@appliedneural.net>";
static char args_doc[] = "";
static char doc[] =
    "blackstrap i2cd -- local and remote network access to the I2C bus"
    "\v"
    "blackstrap i2cd allows clients to send data to, and to receive data "
    "from peripherals on one or more I2C buses on the host hardware. "
    "0mq is used for message passing, and the interface uses an RPC-like "
    "API, passing in parameters as frames. Returned data is similarly "
    "handled, with individual data types passed back as separate frames, "
    "allowing a variety of programs to interface transparently to the "
    "host peripherals in any language or framework that can speak 0mq.";

#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wmissing-field-initializers"
static struct argp_option options[] = {
    {0, 0, 0, 0, "Files & Paths:", 1},
    {
	"config", 'c',
       	"CONFIG",
       	0,
       	"Load configuration from file",
	1
    },

    {0, 0, 0, 0, "Network:", 2},
    {
	"bind", 'b',
       	"BIND",
	0,
       	"Bind to address+port (0mq bind expression)",
	2
    },

    {0, 0, 0, 0, "Process Control:", 3},
    {
	"foreground", 'f',
       	0,
     	0,
       	"Run in foreground (don't daemonize)",
	3
    },
    {
	"kill", 'k',
       	0,
     	0,
       	"Terminate running process",
	3
    },
    {
	"restart", 'r',
       	0,
     	0,
       	"Terminate running process, and run new process",
	3
    },

    // Add a header above the default help parameters
    {0, 0, 0, 0, "Help & Invocation:", -1},

    {0}
};
#pragma clang diagnostic pop

zconfig_t *
options_get_conf(void) {
    return options_conf;
}

void
options_destroy_conf(void) {
    if (options_conf == NULL)
	return;
    zconfig_destroy(&options_conf);
}

static error_t
parse_opt(int key, char *arg,
       	struct argp_state *state __attribute__((unused))) {
    switch(key) {
	case 'c':
	    zconfig_put(options_conf, "configuration/filename", arg);
	    break;
	case 'b':
	    zconfig_put(options_conf, "network/bind", arg);
	    break;
	case 'k':
	    exit(lockfile_kill_process());
	case 'r':
	    if (lockfile_kill_process() < 0)
		return -1;
	    break;
	case 'f':
	    zconfig_put(options_conf, "server/foreground", arg);
	    break;
	default:
	    return ARGP_ERR_UNKNOWN;
    }
    return 0;
}

static struct argp argp = {options, parse_opt, args_doc, doc, 0, 0, 0};

zconfig_t *
options_parse(int argc, char **argv) {
    if (options_conf)
	return options_conf;
    options_conf = zconfig_new("root", NULL);
    argp_parse(&argp, argc, argv, 0, 0, NULL);
    return options_conf;
}

