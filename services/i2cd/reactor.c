
#include <syslog.h>
#include "blackstrap.h"
#include "dispatcher.h"
#include "reactor.h"

static zloop_t *loop;
static zsock_t *frontend;

static int
handle_incoming(zloop_t *zloop __attribute__((unused)), zsock_t *reader,
       	void *arg __attribute__((unused))) {
    dispatch(reader);
    return 0;
}

int
reactor_build(void) {
    int err;
    const char *bind;

    // Prep server socket
    frontend = zsock_new(ZMQ_REP);
    assert(frontend);
    bind = blackstrap_appconf_resolve("network/bind", "tcp://*:1255");
    err = zsock_bind(frontend, "%s", bind);
    if (err == -1) {
	syslog(LOG_ERR, "Could not bind to %s", bind);
	return -1;
    }

    // Build the reactor
    loop = zloop_new();
    assert(loop);

    err = zloop_reader(loop, frontend, handle_incoming, NULL);    
    if (err) {
	syslog(LOG_ERR, "Could not initiate frontend poller for reactor");
	return -1;
    }
    zloop_reader_set_tolerant(loop, frontend);

    return 0;
}

int
reactor_run(void) {
    int err;

    syslog(LOG_INFO, "I2Cd reactor starting");
    err = zloop_start(loop);
    syslog(LOG_INFO, "I2Cd reactor shutdown");

    return err;
}

void
reactor_destroy(void) {
    zloop_reader_end(loop, frontend);
    zsock_destroy(&frontend);
    zloop_destroy(&loop);
}

