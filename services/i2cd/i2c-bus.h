#pragma once

#ifndef I2C_BUS_FD_SIZE
#    define I2C_BUS_FD_SIZE (4)
#endif

/** Read a series of bytes from an I2C peripheral.
 *
 *  bus: the I2C bus device number, as in /dev/i2c-1. On a Raspberry Pi
 *       Model A, bus 0 is on the headers. In a Model B, it's bus 1.
 *  address: the address of the peripheral on the I2C bus. Note that
 *       the address given in many datasheets gives the address shifted
 *       one bit to the left. This is because the I2C device receives
 *       8 bits instead of 7 and expects the LSB to denote a read or write
 *       operation. Linux thinks of these addresses with their normal
 *       7-bit representation. For example, the datasheet for the 
 *       LSM303DLHC Accelerometer/Magnetometer describes its address as
 *       0x32. Meanwhile, a search for the device on the bus with
 *       "i2cdetect -y 1" shows the device at address 0x19. To make
 *       this clear in your code, you might specify the address of this
 *       device explicitely as "0x32 >> 1"
 *  command: I2C devices interpret this to mean different things. Some
 *       devices may interpret it a directive. Most will use the value
 *       to set a pointer to a register from which you may read or write.
 *  buf: pointer to an array of unsigned char, "count" bytes or longer
 *  count: the number of bytes to read from the device.
 *
 *  Returns 0 on success, -1 on failure.
 */
int
i2c_bus_read(unsigned char bus, unsigned char address, unsigned char command,
       	unsigned char *buf, size_t count);

/** Read a single byte from an I2C peripheral.
 *
 *  bus, address, command: (see above)
 *
 *  Returns: positive on success (bottom 8 bits are the data), -1 on failure
 */
int
i2c_bus_read_byte(unsigned char bus, unsigned char address,
	unsigned char command);

/** Write a series of bytes to an I2C peripheral.
 *
 *  bus, address, command: (see above)
 *  data: array of bytes
 *  length: number of bytes to write
 *
 *  Returns: 0 on success, -1 on failure
 */
int
i2c_bus_write(unsigned char bus, unsigned char address, unsigned char command,
       	unsigned char *data, size_t count);

/** Write a single byte to an I2C peripheral.
 *
 *  bus, address, command: (see above)
 *
 *  Returns: 0 on success, -1 on failure
 */
int
i2c_bus_write_byte(unsigned char bus, unsigned char address,
	unsigned char command, unsigned char value);

