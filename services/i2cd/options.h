#pragma once

#include <czmq.h>

/**
 * Parse command line options
 * Creates a static zconfig that can be accessed with get_conf_args()
 */
zconfig_t *
options_parse(int argc, char **argv);

/**
 * Get the zconfig created by parsing options
 * Returns the static zconfig created with options_parse(...),
 * or NULL if options have not been parsed, or parsing failed.
 */
zconfig_t *
options_get_conf(void);

/**
 * Destroys the static zconfig created by options_parse(...)
 */
void
options_destroy_conf(void);

