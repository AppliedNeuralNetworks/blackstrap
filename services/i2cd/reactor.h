#pragma once

int
reactor_build(void);

int
reactor_run(void);

void
reactor_destroy(void);

