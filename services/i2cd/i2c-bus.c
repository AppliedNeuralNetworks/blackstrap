
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <fcntl.h>
#include <sys/ioctl.h>
#include <linux/i2c-dev.h>

#include "blackstrap-log.h"
#include "i2c-bus.h"

static int bus_fd[I2C_BUS_FD_SIZE];
static int bus_fd_initialized;


int i2c_bus_set_slave_address(int fd, unsigned char address);
int i2c_bus_send_command(int fd, unsigned char command);
int i2c_bus_get_fd(unsigned char bus);


int
i2c_bus_read(unsigned char bus, unsigned char address,
	unsigned char command, unsigned char *buf, size_t count) {
    long err;

    int fd = i2c_bus_get_fd(bus);
    if (fd < 0)
	return -1;

    err = i2c_bus_set_slave_address(fd, address);
    if (err < 0)
	return -1;

    err = i2c_bus_send_command(fd, command);
    if (err < 0)
	return -1;

    err = read(fd, buf, count);
    if (err != (int)count) {
	blackstrap_log_error("I2C read failed: bus %d, address 0x%02x, "
		"command 0x%02x (%s)", bus, address, command,
	       	strerror(errno));
	return -1;
    }

    return 0;
}

int
i2c_bus_read_byte(unsigned char bus, unsigned char address,
       	unsigned char command) {
    long err;

    int fd = i2c_bus_get_fd(bus);
    if (fd < 0)
	return -1;

    err = i2c_bus_set_slave_address(fd, address);
    if (err < 0)
	return -1;

    err = i2c_bus_send_command(fd, command);
    if (err < 0)
	return -1;

    unsigned char buf[1];
    err = read(fd, buf, 1);
    if (err != 1) {
	blackstrap_log_error("I2C read byte failed: bus %d, address 0x%02x, "
		"command 0x%02x (%s)", bus, address, command,
	       	strerror(errno));
	return -1;
    }

    return buf[0];
}

int
i2c_bus_write(unsigned char bus, unsigned char address,
	        unsigned char command, unsigned char *data, size_t count) {
    int err;

    if (count > 127) {
	blackstrap_log_error("I2C write: too much data (%zu bytes)", count);
	return -1;
    }

    int fd = i2c_bus_get_fd(bus);
    if (fd < 0)
	return -1;

    err = i2c_bus_set_slave_address(fd, address);
    if (err < 0)
	return -1;

    size_t data_size = count + 1;
    unsigned char buf[256];
    buf[0] = command;
    memcpy(buf + 1, data, count);
    long written_bytes = write(fd, buf, data_size);
    if (written_bytes < 0) {
	blackstrap_log_error("I2C write failed: bus %d, address 0x%02x, "
		"command 0x%02x (%s)", bus, address, command,
	       	strerror(errno));
	return -1;
    }
    if ((size_t)written_bytes != data_size) {
	blackstrap_log_error("I2C short write: bus %d, address 0x%02x, "
		"command 0x%02x (%s)", bus, address, command,
	       	strerror(errno));
	return -1;
    }

    return 0;
}
int
i2c_bus_write_byte(unsigned char bus, unsigned char address,
	        unsigned char command, unsigned char value) {
    long err;

    int fd = i2c_bus_get_fd(bus);
    if (fd < 0)
	return -1;

    err = i2c_bus_set_slave_address(fd, address);
    if (err < 0)
	return -1;

    unsigned char buf[2];
    buf[0] = command;
    buf[1] = value;
    err = write(fd, buf, 2);
    if (err != 2) {
	blackstrap_log_error("I2C write byte failed: bus %d, address 0x%02x, "
		"command 0x%02x, value 0x%02x (%s)", bus, address, command,
	       	value, strerror(errno));
	return -1;
    }

    return 0;
}

int
i2c_bus_set_slave_address(int fd, unsigned char address) {
    int err = ioctl(fd, I2C_SLAVE, address);
    if (err < 0) {
	blackstrap_log_error("I2C failed to set slave address: fd %d, "
		"address 0x%02x", fd, address);
	return -1;
    }

    return 0;
}

int
i2c_bus_send_command(int fd, unsigned char command) {
    long err = write(fd, &command, 1);
    if (err != 1) {
	blackstrap_log_error("I2C command failed: fd %d, command %d",
	       	fd, command);
	return -1;
    }

    return 0;
}

int
i2c_bus_get_fd(unsigned char bus) {
    // Initialize the i2c bus file descriptor table if needed
    if (bus_fd_initialized == 0) {
	for (int i = 0; i < I2C_BUS_FD_SIZE; i++)
	    bus_fd[i] = -1; // -1 signals an unused slot
	bus_fd_initialized = 1;
    }
    int fd = bus_fd[bus];

    // Open device file if necessary
    if (fd < 0) {
	char *device_filename = NULL;
	if (asprintf(&device_filename, "/dev/i2c-%d", bus) < 0) {
	    blackstrap_log_error("Could not build device filename");
	    return -1;
	}
	fd = open(device_filename, O_RDWR, 0);
	free(device_filename);
	if (fd < 0) {
	    blackstrap_log_error("Could not open I2C bus device %d", bus);
	    return -1;
	}
	bus_fd[bus] = fd;
    }

    return fd;
}

