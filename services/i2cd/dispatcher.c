
#include <syslog.h>
#include <string.h>
#include <linux/i2c-dev.h>
#include <blackstrap.h>
#include "dispatcher.h"
#include "i2c-bus.h"

int action_noop(zmsg_t *msg __attribute__((unused)), zsock_t *frontend);
int action_read(zmsg_t *msg, zsock_t *frontend);
int action_read_byte(zmsg_t *msg, zsock_t *frontend);
int action_write(zmsg_t *msg, zsock_t *frontend);
int action_write_byte(zmsg_t *msg, zsock_t *frontend);

int
dispatch(zsock_t *frontend) {
    zmsg_t *msg = zmsg_recv(frontend);
    int64_t action = blackstrap_msg_shift_action(msg);

    int err = 0;
    if (action < 0 || action >= I2CD_ACTION__MAX_)
	return -1;
    switch(action) {
	case(I2CD_ACTION_NOOP):
	    err = action_noop(msg, frontend);
	    break;
	case(I2CD_ACTION_READ):
	    err = action_read(msg, frontend);
	    break;
	case(I2CD_ACTION_READ_BYTE):
	    err = action_read_byte(msg, frontend);
	    break;
	case(I2CD_ACTION_WRITE):
	    err = action_write(msg, frontend);
	    break;
	case(I2CD_ACTION_WRITE_BYTE):
	    err = action_write_byte(msg, frontend);
	    break;
    }
    zmsg_destroy(&msg);
    return err;
}

int
action_noop(zmsg_t *msg __attribute__((unused)), zsock_t *frontend) {
    blackstrap_send_status_success(frontend, ZFRAME_MORE);
    blackstrap_send_time(frontend, NULL, ZFRAME_FINAL);

    return 0;
}

int
action_read(zmsg_t *msg, zsock_t *frontend) {
    uint8_t bus, address, command, length;
    bus     = blackstrap_msg_shift_byte(msg);
    address = blackstrap_msg_shift_byte(msg);
    command = blackstrap_msg_shift_byte(msg);
    length  = blackstrap_msg_shift_byte(msg);

    int err;
    uint8_t buf[256];
    err = i2c_bus_read(bus, address, command, buf, length);
    if (err < 0) {
	blackstrap_send_status_failure(frontend);
	return -1;
    }

    blackstrap_send_status_success(frontend, ZFRAME_MORE);
    blackstrap_send_time(frontend, NULL, ZFRAME_MORE);
    blackstrap_send_bytes(frontend, buf, length, ZFRAME_FINAL);

    return 0;
}

int
action_read_byte(zmsg_t *msg, zsock_t *frontend) {
    uint8_t bus, address, command;
    bus     = blackstrap_msg_shift_byte(msg);
    address = blackstrap_msg_shift_byte(msg);
    command = blackstrap_msg_shift_byte(msg);

    errno = 0;
    uint8_t value = (uint8_t)i2c_bus_read_byte(bus, address, command);
    if (errno) {
	blackstrap_send_status_failure(frontend);
	return -1;
    }

    blackstrap_send_status_success(frontend, ZFRAME_MORE);
    blackstrap_send_time(frontend, NULL, ZFRAME_MORE);
    blackstrap_send_byte(frontend, value, ZFRAME_FINAL);

    return 0;
}

int
action_write(zmsg_t *msg, zsock_t *frontend) {
    uint8_t bus, address, command;
    bus = blackstrap_msg_shift_byte(msg);
    address = blackstrap_msg_shift_byte(msg);
    command = blackstrap_msg_shift_byte(msg);

    zframe_t *data_frame = zmsg_pop(msg);

    int err = i2c_bus_write(bus, address, command,
	    zframe_data(data_frame), zframe_size(data_frame));

    free(data_frame);
    blackstrap_send_status(frontend, err, ZFRAME_FINAL);

    return err;
}

int
action_write_byte(zmsg_t *msg, zsock_t *frontend) {
    uint8_t bus, address, command, value;
    bus = blackstrap_msg_shift_byte(msg);
    address = blackstrap_msg_shift_byte(msg);
    command = blackstrap_msg_shift_byte(msg);
    value = blackstrap_msg_shift_byte(msg);

    int err = i2c_bus_write_byte(bus, address, command, value);
    blackstrap_send_status(frontend, err, ZFRAME_FINAL);

    return err;
}


