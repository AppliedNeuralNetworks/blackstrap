
#include "blackstrap.h"
#include "options.h"
#include "reactor.h"

int
main(int argc, char *argv[]) {
    zconfig_t *conf_options = NULL;

    // Parse command line and store into configuration stack
    conf_options = options_parse(argc, argv);
    blackstrap_appconf_level_set(conf_options, BLACKSTRAP_APPCONF_OPTIONS);

    // Initialize the configuration with app path info, etc.
    blackstrap_init();
    
    if (!blackstrap_appconf_resolve("server/foreground", NULL))
	if (zsys_daemonize("/var/tmp")) {
	    blackstrap_log_error("Could not move process to background");
	    exit(EXIT_FAILURE);
	}

    if (lockfile_acquire() < 0) {
	blackstrap_log_error("Could not acquire process exclusive lock");
        exit(EXIT_FAILURE);
    }

    // Build and run the reactor
    if (reactor_build() < 0) {
        blackstrap_log_error("Could not build reactor");
        return -1;
    }
    reactor_run();
    reactor_destroy();

    lockfile_release();
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

