
#import os
import os.path
#import sys
#import argparse
import copy
from string import Template
from os.path import join

from . import conf
from .i2cd import I2Cd

class Application(object):
    conf_bootstrap_template = {
        'conf_file_name':       '$app_name.yaml',
        'conf_system_dir':      '/etc',
        'conf_user_dir':        '~/.config',
        'conf_project_dir':     '/srv/project',
        # If no configuration file is specified on the command line, this
        # built-in list of locations is searched. For safety, do not include
        # the current directory.
        'conf_file_locations': [
            '$conf_user_dir/$app_name',         # User home directory
            '$app_dir/conf',                    # Self-contained
            '$conf_project_dir/$app_name/conf', # FHS services heirarchy
            '/etc/$app_name',                   # Ubuntu & Debian-like
            '/etc',                             # Red Hat, SuSE
        ]
    }

    conf_app_defaults = { }

    def __init__(self, app_dir):
        #-- Store parameters
        self.app_dir = app_dir

        #-- Copy bootstrap configuration
        self.conf_bootstrap = copy.deepcopy(
            self.__class__.conf_bootstrap_template)

        #-- Populate computed values
        self.conf_bootstrap.update(
            app_name=os.path.basename(app_dir),
            app_dir=app_dir,
        )

        #-- Inner def to apply substitutions from config
        def interpolate(value):
            return Template(value).safe_substitute(self.conf_bootstrap)

        #-- Interpolate configuration back into itself
        for key, value in list(self.conf_bootstrap.items()):
            if type(value) is str:
                self.conf_bootstrap[key] = interpolate(value)
            elif type(value) is list:
                for i in range(0, len(value)):
                    value[i] = interpolate(value[i])

        #-- Create stack of configurations
        confs = [
            # generated conf will be in first position
            self.__class__.conf_app_defaults,
            self.conf_bootstrap
        ]

        #-- Hunt for configuration file
#        #   If a configuration file was specified on the command line, load it.
#        if self.options.conf_file:
#            conf_from_file = conf.ConfYAML(self.options.conf_file,
#                                             ignore_changes=True)
#        else:
#            #-- Otherwise, let the hunter try to find it.
#            try:
#                conf_from_file = conf.ConfHunterFactory(
#                    conf.ConfYAML,
#                    Application.conf_file_name,
#                    self.bootstrap.conf['conf_file_locations'],
#                    ignore_changes=True
#                )
#                confs.insert(0, conf_from_file)
#            except conf.ConfNotFoundError:
#                pass

        try:
            conf_from_file = conf.ConfHunterFactory(
                conf.ConfYAML,
                self.conf_bootstrap['conf_file_name'],
                self.conf_bootstrap['conf_file_locations'],
            )
            confs.insert(0, conf_from_file)
        except conf.ConfNotFoundError:
            print("No configuration file found.\n")

        #-- Build ConfStack
        self.conf = conf.ConfStack(
            confs,
            #optparse=self.options.__dict__
        )

    def rel_dir(self, *subdirs):
        return os.path.join(self.base_dir, *subdirs)

    def run(self):
        print("This would be Python IMU. Call your code here.")
        accel = I2Cd("tcp://172.16.5.0:1255")
        print(accel.read_byte(1, 0x32 >> 1, 0x0f))


