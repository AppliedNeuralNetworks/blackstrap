
import datetime
import zmq
import struct
from . import blackstrap_wire_pb2 as wire

class I2CdError(Exception):
    pass

I2CD_ACTION = {
    'NOOP': 0,

    'READ': 1,
    'READ_BYTE': 2,
    'WRITE': 3,
    'WRITE_BYTE': 4,

    '_MAX_': 5,
}

class I2Cd(object):
    def __init__(self, endpoint):
        ''' endpoint: zmq-style endpoint URI, such as "tcp://172.16.5.0:1255"
        '''
        self.endpoint = endpoint
        self.ctx = zmq.Context()
        self.socket = self.ctx.socket(zmq.REQ)
        self.socket.connect(endpoint)

    def get_endpoint(self):
        return self.endpoint

    def get_context(self):
        return self.ctx

    def get_socket(self):
        return self.socket

    def noop(self):
        ''' returns: timestamp (float of seconds/nanoseconds since epoch)
        '''
        action = wire.SInt64()
        action.val = I2CD_ACTION['NOOP']
        self.socket.send(action.SerializeToString())

        reply = self.socket.recv_multipart()
        status = wire.SInt64()
        status.ParseFromString(reply[0])
        if status.val < 0:
            raise I2CdError('noop: I2Cd reported failure status')

        time = wire.Time()
        time.ParseFromString(reply[1])
        timestamp = datetime.datetime.fromtimestamp(
            time.sec).replace(microsecond=time.nsec/1000)

        return timestamp

    def read(self, bus, address, command, length):
        ''' bus:     I2C bus number, such as 1 (to correspond with /dev/i2c-1)
            address: address of peripheral on the bus, 7 bit value.
                     If you're using the manufacturer datasheet address, you
                     may need to right shift this value, such as 0x32 >> 1
            command: a command or register on the i2c peripheral
            length:  number of bytes to read (0-127), as a python integer

            returns: 2-tuple of: datetime, n-tuple of byte values
        '''
        try:
            timestamp, bytestring = self.read_raw(bus, address, command,
                                                  length)
        except I2CdError:
            raise I2CdError('read: I2Cd reported failure status')
        values = struct.unpack('B'*length, bytestring)
        return timestamp, values

    def read_raw(self, bus, address, command, length):
        ''' bus:     I2C bus number, such as 1 (to correspond with /dev/i2c-1)
            address: address of peripheral on the bus, 7 bit value.
                     If you're using the manufacturer datasheet address, you
                     may need to right shift this value, such as 0x32 >> 1
            command: a command or register on the i2c peripheral
            length:  number of bytes to read (0-127), as a python integer

            returns: 2-tuple of: datetime, string of byte values
        '''
        _action = wire.SInt64()
        _action.val = I2CD_ACTION['READ']
        _bus = wire.Bytes()
        _bus.data = struct.pack('B', bus)
        _address = wire.Bytes()
        _address.data = struct.pack('B', address)
        _command = wire.Bytes()
        _command.data = struct.pack('B', command)
        _length = wire.Bytes()
        _length.data = struct.pack('B', length)
        self.socket.send_multipart((
            _action.SerializeToString(),
            _bus.SerializeToString(),
            _address.SerializeToString(),
            _command.SerializeToString(),
            _length.SerializeToString(),
        ))

        reply = self.socket.recv_multipart()
        status = wire.SInt64()
        status.ParseFromString(reply[0])
        if status.val < 0:
            raise I2CdError('read_raw: I2Cd reported failure status')

        time = wire.Time()
        time.ParseFromString(reply[1])
        timestamp = datetime.datetime.fromtimestamp(
            time.sec).replace(microsecond=int(time.nsec/1000))

        _bytes = wire.Bytes()
        _bytes.ParseFromString(reply[2])
        return timestamp, _bytes.data

    def read_byte(self, bus, address, command):
        ''' bus:     I2C bus number, such as 1 (to correspond with /dev/i2c-1)
            address: address of peripheral on the bus, 7 bit value.
                     If you're using the manufacturer datasheet address, you
                     may need to right shift this value, such as 0x32 >> 1
            command: a command or register on the i2c peripheral

            returns: 2-tuple of: datetime, single byte as int()
        '''
        _action = wire.SInt64()
        _action.val = I2CD_ACTION['READ_BYTE']
        _bus = wire.Bytes()
        _bus.data = struct.pack('B', bus)
        _address = wire.Bytes()
        _address.data = struct.pack('B', address)
        _command = wire.Bytes()
        _command.data = struct.pack('B', command)
        self.socket.send_multipart((
            _action.SerializeToString(),
            _bus.SerializeToString(),
            _address.SerializeToString(),
            _command.SerializeToString(),
        ))

        reply = self.socket.recv_multipart()
        status = wire.SInt64()
        status.ParseFromString(reply[0])
        if status.val < 0:
            raise I2CdError('read_byte: I2Cd reported failure status')

        time = wire.Time()
        time.ParseFromString(reply[1])
        timestamp = datetime.datetime.fromtimestamp(
            time.sec).replace(microsecond=int(time.nsec/1000))

        _bytes = wire.Bytes()
        _bytes.ParseFromString(reply[2])
        value = struct.unpack('B', _bytes.data)

        return timestamp, value[0]

    def write(self, bus, address, command, data):
        ''' bus:     I2C bus number, such as 1 (to correspond with /dev/i2c-1)
            address: address of peripheral on the bus, 7 bit value.
                     If you're using the manufacturer datasheet address, you
                     may need to right shift this value, such as 0x32 >> 1
            command: a command or register on the i2c peripheral
            data:    iterable containing integers in the range 0-255
        '''
        _action = wire.SInt64()
        _action.val = I2CD_ACTION['WRITE']
        _bus = wire.Bytes()
        _bus.data = struct.pack('B', bus)
        _address = wire.Bytes()
        _address.data = struct.pack('B', address)
        _command = wire.Bytes()
        _command.data = struct.pack('B', command)
        _data = wire.Bytes()
        _data.data = struct.pack('B' * len(data), *data)

        self.socket.send_multipart((
            _action.SerializeToString(),
            _bus.SerializeToString(),
            _address.SerializeToString(),
            _command.SerializeToString(),
            _data.SerializeToString(),
        ))

        reply = self.socket.recv_multipart()
        status = wire.SInt64()
        status.ParseFromString(reply[0])
        if status.val < 0:
            raise I2CdError('write: I2Cd reported failure status')

    def write_byte(self, bus, address, command, value):
        ''' bus:     I2C bus number, such as 1 (to correspond with /dev/i2c-1)
            address: address of peripheral on the bus, 7 bit value.
                     If you're using the manufacturer datasheet address, you
                     may need to right shift this value, such as 0x32 >> 1
            command: a command or register on the i2c peripheral
            value:   single byte value, 0-255, as a python integer
        '''
        _action = wire.SInt64()
        _action.val = I2CD_ACTION['WRITE_BYTE']
        _bus = wire.Bytes()
        _bus.data = struct.pack('B', bus)
        _address = wire.Bytes()
        _address.data = struct.pack('B', address)
        _command = wire.Bytes()
        _command.data = struct.pack('B', command)
        _data = wire.Bytes()
        _data.data = struct.pack('B', value)

        self.socket.send_multipart((
            _action.SerializeToString(),
            _bus.SerializeToString(),
            _address.SerializeToString(),
            _command.SerializeToString(),
            _data.SerializeToString(),
        ))

        reply = self.socket.recv_multipart()
        status = wire.SInt64()
        status.ParseFromString(reply[0])
        if status.val < 0:
            raise I2CdError('write_byte: I2Cd reported failure status')


