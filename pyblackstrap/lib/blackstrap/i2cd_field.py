
import sys
import os
from os.path import realpath, dirname, join
import re
import struct
import datetime
import zmq
import threading

from .conf import ConfYAML
from .i2cd import I2Cd

class I2CdField(object):
    def __init__(self, device_name):
        # Determine paths
        script_dir = dirname(sys.argv[0])
        proj_dev_conf_dir = join(script_dir, '..', '..', 'conf')
        proj_dev_conf = "%s/devices/%s.yaml" % (proj_dev_conf_dir, device_name)
        sys_dev_conf_dir = '/etc/blackstrap/conf'
        sys_dev_conf = '%s/devices/%s/yaml' % (sys_dev_conf_dir, device_name)

        # Load device configuration
        if os.access(proj_dev_conf, os.R_OK):
            self.conf = ConfYAML(proj_dev_conf)
        elif os.access(sys_dev_conf, os.R_OK):
            self.conf = ConfYAML(sys_dev_conf)
        else:
            raise RuntimeError("Device configuration not found for %s" %
                               device_name)

    def get(self, i2cd, bus, field_name):
        field = self.conf['fields'][field_name]
        address = self.conf['address']
        register = field['register']
        timestamp, value = i2cd.read_byte(bus, address >> 1, register)

        width = field.get('width', 8)
        if width == 8:
            return timestamp, data

        shift = field.get('shift', 0)
        mask = ((1 << width) - 1) << shift

        return timestamp, (value & mask) >> shift

    def get_str(self, i2cd, bus, field_name):
        descr = self.conf['fields'][field_name]['descr']
        value, timestamp = self.get(i2cd, bus, field_name)
        for k, v in descr.iteritems():
            if v == value:
                return timestamp, k
        return timestamp, str(value)

    def set(self, i2cd, bus, field_name, value):
        field = self.conf['fields'][field_name]
        address = self.conf['address']
        register = field['register']
        width = field.get('width', 8)

        if width == 8:
            i2cd.write_byte(bus, address >> 1, register, value)
            return

        timestamp, old_value = i2cd.read_byte(bus, address >> 1, register)

        shift = field.get('shift', 0)
        mask = ~(((1 << width) - 1) << shift)
        new_value = (old_value & mask) | (value << shift)

        i2cd.write_byte(bus, address >> 1, register, new_value)

    def set_str(self, i2cd, bus, field_name, descr):
        value = self.conf['fields'][field_name]['descr'][descr]
        self.set(i2cd, bus, field_name, value)

    def get_range_int16(self, i2cd, bus, range_name):
        address = self.conf['address']
        auto_increment = self.conf.get('auto_increment', 0)

        range_ = self.conf['int16_ranges'][range_name]
        length = range_['length']
        register = range_['register']
        if not auto_increment:
            register |= 0b10000000

        byte_order = range_.get('byte_order', 'hl')
        if byte_order == 'lh':
            byte_format = "<%dh" % length
        else:
            byte_format = ">%dh" % length

        timestamp, bytestring = i2cd.read_raw(bus, address >> 1, register,
                                              length*2)
        values = struct.unpack(byte_format, bytestring)
        return timestamp, values

