
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <stdio.h>
#include <strings.h>
#include <czmq.h>

#include "blackstrap.h"

zconfig_t *blackstrap_i2cd_field_locate(zconfig_t *conf,
       	const char *field_name);
long blackstrap_i2cd_field_ranged_resolve(zconfig_t *field, const char *path,
       	int min, int max);


zconfig_t *
blackstrap_i2cd_field_load(const char *device_name) {
    const char *path = NULL;
    zconfig_t *conf = NULL;
   
    // Absolute path
    if (device_name[0] == '/') {
	path = device_name;
	conf = blackstrap_conf_load_yaml(path);
	if (!conf)
	    blackstrap_log_error_exit("Device file \"%s\" could not be "
		    "loaded: %s", device_name, strerror(errno));
	return conf;
    }

    blackstrap_pathlist_t *pathlist = blackstrap_pathlist_new();

    const char *project_dir = blackstrap_appconf_resolve(
            "pathinfo/project_dir", NULL);
    if (project_dir)
	blackstrap_pathlist_append(pathlist, "%s/conf/devices/%s.yaml",
		project_dir, device_name);
    blackstrap_pathlist_append(pathlist, "/srv/project/blackstrap/"
	    "conf/devices/%s.yaml", device_name);
    blackstrap_pathlist_append(pathlist, "/etc/%s/devices/%s.yaml",
	    program_invocation_short_name, device_name);
    blackstrap_pathlist_append(pathlist, "/etc/blackstrap/devices/%s.yaml",
            device_name);

    const char *filename = blackstrap_pathlist_first_existing(pathlist);

    if (!filename) {
        blackstrap_pathlist_destroy(&pathlist);
	blackstrap_log_error_exit("Device file for \"%s\" could not be found",
	       	device_name);
    }

    conf = blackstrap_conf_load_yaml(filename);
    if (!conf) {
        blackstrap_log_error("Device configuration \"%s\" was found "
		"but could not be loaded", filename);
        blackstrap_pathlist_destroy(&pathlist);
        exit(EXIT_FAILURE);
    }

    return conf;
}

void
blackstrap_i2cd_field_unload(zconfig_t **conf) {
    zconfig_destroy(conf);
}

int
blackstrap_i2cd_field_get(zsock_t *sock, zconfig_t *conf, unsigned char bus,
       	const char *field_name, struct timespec *timestamp) {
    // Fetch device address
    long address = blackstrap_i2cd_field_ranged_resolve(conf, "address",
	    0, 255);
    if (address < 0)
	return -1;
    address = address >> 1;

    // Select specific field
    zconfig_t *field = blackstrap_i2cd_field_locate(conf, field_name);
    if (!field)
	return -1;

    long reg = blackstrap_i2cd_field_ranged_resolve(field, "register", 0, 255);
    if (reg < 0)
	return -1;

    // Fetch current value from device
    int value = blackstrap_i2cd_read_byte(sock, bus, (unsigned char)address,
	    (unsigned char)reg, timestamp);
    if (value < 0)
	return -1;

    long width = blackstrap_i2cd_field_ranged_resolve(field, "width", 1, 8);
    if (width < 0)
	width = 8;

    if (width == 8)
	return value;

    long shift = blackstrap_i2cd_field_ranged_resolve(field, "shift", 1, 8);
    if (shift < 0)
	shift = 0;

    unsigned char mask = (unsigned char)(((1 << width) - 1) << shift);

    return (value & mask) >> shift;
}

char *
blackstrap_i2cd_field_str_get(zsock_t *sock, zconfig_t *conf,
       	unsigned char bus, const char *field_name,
       	struct timespec *timestamp) {
    int value = blackstrap_i2cd_field_get(sock, conf, bus, field_name,
	    timestamp);
    if (value < 0)
	return NULL;

    char *descr_path;
    if (asprintf(&descr_path, "fields/%s/descr", field_name) < 0)
	return blackstrap_stringify_uchar((unsigned char)value);
    zconfig_t *descr = zconfig_locate(conf, descr_path);
    free(descr_path);
    if (!descr) 
	return blackstrap_stringify_uchar((unsigned char)value);

    zconfig_t *cursor;
    cursor = zconfig_child(descr);
    while (cursor) {
	if(strtol(zconfig_value(cursor), NULL, 0) == value)
	    return strdup(zconfig_name(cursor));
	cursor = zconfig_next(cursor);
    }
    return blackstrap_stringify_uchar((unsigned char)value);
}

int
blackstrap_i2cd_field_set(zsock_t *sock, zconfig_t *conf, unsigned char bus,
       	const char *field_name, unsigned char value) {
    // Fetch device address
    long address = blackstrap_i2cd_field_ranged_resolve(conf, "address",
	    0, 255);
    if (address < 0) {
	blackstrap_log_error(
		"i2cd_field_set: address not found or out of range");
	return -1;
    }
    address = address >> 1;

    // Select specific field
    zconfig_t *field = blackstrap_i2cd_field_locate(conf, field_name);
    if (!field) {
	blackstrap_log_error("i2cd_field_set: field %s not found",
		field_name);
	return -1;
    }

    long reg = blackstrap_i2cd_field_ranged_resolve(field, "register", 0, 255);
    if (reg < 0) {
	blackstrap_log_error("i2cd_field_set: register not found in "
		"field %s, or out of range", field_name);
	return -1;
    }

    long width = blackstrap_i2cd_field_ranged_resolve(field, "width", 1, 8);
    if (width < 0)
	width = 8;
    if (width == 8)
	return blackstrap_i2cd_write_byte(sock, bus, (unsigned char)address,
		(unsigned char)reg, value);

    long shift = blackstrap_i2cd_field_ranged_resolve(field, "shift", 0, 7);
    if (shift < 0)
	shift = 0;

    // Fetch current value from device
    int data = blackstrap_i2cd_read_byte(sock, bus, (unsigned char)address,
	    (unsigned char)reg, NULL);
    if (data < 0) {
	blackstrap_log_error("i2cd_field_set: read failed");
	return -1;
    }

    unsigned char mask = (unsigned char)(~(((1 << width) - 1) << shift));
    unsigned char new_value = (unsigned char)((data & mask) |
	    (value << shift));

    return blackstrap_i2cd_write_byte(sock, bus, (unsigned char)address,
	    (unsigned char)reg, new_value);
}

int
blackstrap_i2cd_field_str_set(zsock_t *sock, zconfig_t *conf,
       	unsigned char bus, const char *field_name, const char *descr) {
    char *descr_path;
    if (asprintf(&descr_path, "fields/%s/descr/%s", field_name, descr) < 0)
	return -1;

    long value = blackstrap_i2cd_field_ranged_resolve(conf, descr_path,
	    0, 255);
    free(descr_path);
    if (value < 0) {
	blackstrap_log_error("i2cd_field_str_set: descr %s not found in "
		"field %s, or out of range", descr, field_name);
	return -1;
    }

    return blackstrap_i2cd_field_set(sock, conf, bus, field_name, 
	    (unsigned char)value);
}

long
blackstrap_i2cd_field_get_range_int16(int16_t **buf, zsock_t *sock,
       	zconfig_t *conf, unsigned char bus, const char *range_name) {
    if (*buf) {
	blackstrap_log_error("i2cd_field_get_range_int16: buf pointer "
		"not NULL");
	return -1;
    }

    char *range_path;
    if (asprintf(&range_path, "int16_ranges/%s", range_name) < 0)
	return -1;
    zconfig_t *range = zconfig_locate(conf, range_path);
    free(range_path);
    if (!range)
	return -1;

    long address = blackstrap_i2cd_field_ranged_resolve(conf, "address",
	    0, 255);
    if (address < 0) {
	blackstrap_log_error("i2cd_field_range: address not found "
		"in range %s, or out of range", range_name);
	return -1;
    }

    long length = blackstrap_i2cd_field_ranged_resolve(range, "length",
	    1, 127);
    if (length < 0) {
	blackstrap_log_error("i2cd_field_range: length not found "
		"in range %s, or out of range", range_name);
	return -1;
    }

    long reg = blackstrap_i2cd_field_ranged_resolve(range, "register",
	    0, 127);
    if (reg < 0) {
	blackstrap_log_error("i2cd_field_range: register not found "
		"in range %s, or out of range", range_name);
	return -1;
    }

    long auto_increment = blackstrap_i2cd_field_ranged_resolve(conf,
	    "auto_increment", 0, 1);
    if (auto_increment < 0)
	auto_increment = 0;
    if (!auto_increment)
	reg |= 128; // some devices use MSB to explicitely signal autoincrement

    const char *byte_order = zconfig_resolve(range, "byte_order", "hl");
    int byte_order_is_reversed = !strcasecmp(byte_order, "lh");

    unsigned char data[256];
    int ret = blackstrap_i2cd_read(sock, bus, (unsigned char)address >> 1,
	    (unsigned char)reg, data, (unsigned char)length*2, NULL);
    if (ret < 0) {
	blackstrap_log_error("i2cd_field_range: read() failed");
	return -1;
    }

    *buf = realloc(*buf, (size_t)length * sizeof (int16_t));
    for (int i = 0; i < length; i++) {
	unsigned char msb, lsb;
	if (byte_order_is_reversed) {
	    lsb = data[i*2];
	    msb = data[(i*2)+1];
	} else {
	    lsb = data[(i*2)+1];
	    msb = data[i*2];
	}
	(*buf)[i] = (int16_t)((msb << 8) | lsb);
    }

    return length;
}

zconfig_t *
blackstrap_i2cd_field_locate(zconfig_t *conf, const char *field_name) {
    char *field_path;
    if (asprintf(&field_path, "fields/%s", field_name) < 0)
	return NULL;

    zconfig_t *field = zconfig_locate(conf, field_path);
    free(field_path);

    return field;
}

long
blackstrap_i2cd_field_ranged_resolve(zconfig_t *field, const char *path,
       	int min, int max) {
    long value;
    int err = blackstrap_conf_resolve_long(&value, field, path);
    if (err < 0)
	return -1;
    if (value < min || value > max)
	return -ERANGE;
    return value;
}

