
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <stdio.h>
#include <yaml.h>

#include "blackstrap.h"

static int stream_ended = 0;

int conf_parse_yaml_layer(yaml_parser_t *parser, zconfig_t *parent,
	yaml_event_type_t parent_event_type);


zconfig_t *
blackstrap_conf_load(const char *filename) {
    if (access(filename, R_OK) < 0)
	return NULL;
    zconfig_t *conf = zconfig_load(filename);
    if (!conf)
	return NULL;

    return conf;
}

int
conf_parse_yaml_layer(yaml_parser_t *parser, zconfig_t *parent,
       	yaml_event_type_t parent_event_type) {
    yaml_event_t event;
    int err;
    char *value = NULL;
    char *this = NULL;

    while (!stream_ended) {
	if (!yaml_parser_parse(parser, &event)) {
	    blackstrap_log_error("Parse error");
	    stream_ended = 1;
	    return -1;
	}

	switch(event.type)
	{ 
	    case YAML_NO_EVENT:
	    case YAML_STREAM_START_EVENT:
	    case YAML_DOCUMENT_START_EVENT:
	    case YAML_ALIAS_EVENT:
		break;

	    case YAML_STREAM_END_EVENT:
	    case YAML_DOCUMENT_END_EVENT:
		stream_ended = 1;
		break;

	    case YAML_SEQUENCE_START_EVENT:
		if (!this) {
		    if (conf_parse_yaml_layer(parser, parent, event.type) < 0)
			return -1;
		}
		else {
		    zconfig_put(parent, this, NULL);
		    err = conf_parse_yaml_layer(parser,
			    zconfig_locate(parent, this), event.type);
		    zstr_free(&this);
		    if (err < 0)
			return -1;
		}
	       	break;
	    case YAML_SEQUENCE_END_EVENT:
		return 0;

	    case YAML_MAPPING_START_EVENT:
		if (!this) {
		    if (conf_parse_yaml_layer(parser, parent, event.type) < 0)
			return -1;
		}
		else {
		    zconfig_put(parent, this, NULL);
		    err = conf_parse_yaml_layer(parser,
			    zconfig_locate(parent, this), event.type);
		    zstr_free(&this);
		    if (err < 0)
			return -1;
		}
	      	break;

	    case YAML_MAPPING_END_EVENT:
		return 0;

	    case YAML_SCALAR_EVENT:
		value = (char *)event.data.scalar.value;
		// Mappings
		if (parent_event_type == YAML_MAPPING_START_EVENT) {
		    if (!this)
			this = strdup(value);
		    else {
			zconfig_put(parent, this, value);
			zstr_free(&this);
		    }
		}
		// Sequences
		else
		if (parent_event_type == YAML_SEQUENCE_START_EVENT)
		    zconfig_put(parent, value, NULL);
	}
	yaml_event_delete(&event);
    }
    return 0;
}

zconfig_t *
blackstrap_conf_load_yaml(const char *file_name) {
    FILE *file = fopen(file_name, "r");
    if (!file) {
	blackstrap_log_error("Could not open config file %s", file_name);
	return NULL;
    }

    yaml_parser_t parser;
    if(!yaml_parser_initialize(&parser)) {
	blackstrap_log_error("Could not initialize YAML parser");
	return NULL;
    }
    yaml_parser_set_input_file(&parser, file);

    zconfig_t *conf = zconfig_new("root", NULL);
    stream_ended = 0;
    int result = conf_parse_yaml_layer(&parser, conf, 0);
    fclose(file);
    yaml_parser_delete(&parser);

    if (result < 0) {
	zconfig_destroy(&conf);
	blackstrap_log_error("Unable to load YAML file %s", file_name);
	return NULL;
    }

    return conf;
}

int
blackstrap_conf_resolve_long(long *dest, zconfig_t *conf, const char *path) {
    const char *str = zconfig_resolve(conf, path, NULL);
    if (!str)
	return -1;

    errno = 0;
    *dest = strtol(str, NULL, 0);
    if (errno)
	return -errno;

    return 0;
}

int
blackstrap_conf_resolve_long_default(long *dest, zconfig_t *conf,
       	const char *path, long default_value) {
    int err = blackstrap_conf_resolve_long(dest, conf, path);
    if (err == -1) {
	*dest = default_value;
	return 0;
    }

    return err;
}

