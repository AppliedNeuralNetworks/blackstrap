
#include <errno.h>
#include <time.h>
#include <czmq.h>
#include "blackstrap-wire.pb-c.h"

#include "blackstrap-protocol.h"

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ send frame ]

int blackstrap_send_int64(zsock_t *sock, int64_t value, int flags) {
    Blackstrap__Wire__SInt64 data = BLACKSTRAP__WIRE__SINT64__INIT;
    data.val = value;

    size_t len = blackstrap__wire__sint64__get_packed_size(&data);
    uint8_t *buf = malloc(len);
    blackstrap__wire__sint64__pack(&data, buf);

    zframe_t *frame = zframe_new(buf, len);
    int err = zframe_send(&frame, sock, flags);
    zframe_destroy(&frame);
    free(buf);

    return err;
}

int
blackstrap_send_bytes(zsock_t *sock, uint8_t *data, size_t length,
       	int flags) {
    Blackstrap__Wire__Bytes bytes = BLACKSTRAP__WIRE__BYTES__INIT;
    bytes.data.data = data;
    bytes.data.len = length;

    size_t len = blackstrap__wire__bytes__get_packed_size(&bytes);
    uint8_t *buf = malloc(len);

    blackstrap__wire__bytes__pack(&bytes, buf);

    zframe_t *frame = zframe_new(buf, len);
    int err = zframe_send(&frame, sock, flags);
    zframe_destroy(&frame);
    free(buf);

    return err;
}

int blackstrap_send_byte(zsock_t *sock, uint8_t value, int flags) {
    return blackstrap_send_bytes(sock, &value, 1, flags);
}

int blackstrap_send_time(zsock_t *sock, struct timespec *tp, int flags) {
    struct timespec _tp;
    if (!tp) {
	tp = &_tp;
	clock_gettime(CLOCK_REALTIME, tp);
    }

    Blackstrap__Wire__Time time = BLACKSTRAP__WIRE__TIME__INIT;
    time.sec = tp->tv_sec;
    time.nsec = tp->tv_nsec;

    size_t len = blackstrap__wire__time__get_packed_size(&time);
    uint8_t *buf = malloc(len);
    blackstrap__wire__time__pack(&time, buf);

    zframe_t *frame = zframe_new(buf, len);
    int err = zframe_send(&frame, sock, flags);
    zframe_destroy(&frame);
    free(buf);

    return err;
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - [ shift frame ]

int blackstrap_msg_shift_status(zmsg_t *msg) {
    return (int)blackstrap_msg_shift_int64(msg);
}

int blackstrap_msg_shift_action(zmsg_t *msg) {
    return (int)blackstrap_msg_shift_int64(msg);
}

void blackstrap_msg_shift(zmsg_t *msg) {
    zframe_t *frame = zmsg_pop(msg);
    zframe_destroy(&frame);
}

size_t blackstrap_msg_bytes_get_len(zmsg_t *msg) {
    zframe_t *frame = zmsg_first(msg);
    Blackstrap__Wire__Bytes *bytes = blackstrap__wire__bytes__unpack(
	    NULL, zframe_size(frame), zframe_data(frame));
    size_t len = bytes->data.len;
    blackstrap__wire__bytes__free_unpacked(bytes, NULL);

    return len;
}

size_t blackstrap_msg_shift_bytes(zmsg_t *msg, uint8_t *buf) {
    zframe_t *frame = zmsg_pop(msg);
    Blackstrap__Wire__Bytes *bytes = blackstrap__wire__bytes__unpack(
	    NULL, zframe_size(frame), zframe_data(frame));
    size_t len = bytes->data.len;
    memcpy(buf, bytes->data.data, bytes->data.len);
    blackstrap__wire__bytes__free_unpacked(bytes, NULL);
    zframe_destroy(&frame);

    return len;
}

uint8_t blackstrap_msg_shift_byte(zmsg_t *msg) {
    uint8_t value = 0;
    blackstrap_msg_shift_bytes(msg, &value);

    return value;
}

int64_t blackstrap_msg_shift_int64(zmsg_t *msg) {
    zframe_t *frame = zmsg_pop(msg);
    Blackstrap__Wire__SInt64 *data = blackstrap__wire__sint64__unpack(
	    NULL, zframe_size(frame), zframe_data(frame));
    int64_t value = data->val;
    blackstrap__wire__sint64__free_unpacked(data, NULL);
    zframe_destroy(&frame);

    return value;
}

int blackstrap_msg_shift_time(zmsg_t *msg, struct timespec *tp) {
    if (!tp)
	return -1;
    zframe_t *frame = zmsg_pop(msg);
    Blackstrap__Wire__Time *data = blackstrap__wire__time__unpack(
	    NULL, zframe_size(frame), zframe_data(frame));
    tp->tv_sec = (time_t)(data->sec);
    tp->tv_nsec = (long)(data->nsec);
    blackstrap__wire__time__free_unpacked(data, NULL);
    zframe_destroy(&frame);

    return 0;
}

// - - - - - - - - - - - - - - - - - - - - - - - - [ convenience and clarity ]

int blackstrap_recv_status(zsock_t *sock) {
    zmsg_t *reply = zmsg_recv(sock);
    int64_t status = blackstrap_msg_shift_status(reply);
    zmsg_destroy(&reply);
    return (int)status;
}

int blackstrap_send_status(zsock_t *sock, int status, int flags) {
    return blackstrap_send_int64(sock, status, flags);
}

int blackstrap_send_status_success(zsock_t *sock, int flags) {
    return blackstrap_send_status(sock, 0, flags);
}

int blackstrap_send_status_failure(zsock_t *sock) {
    return blackstrap_send_status(sock, -1, 0);
}

int blackstrap_send_action(zsock_t *sock, int action, int flags) {
    return blackstrap_send_int64(sock, action, flags);
}

