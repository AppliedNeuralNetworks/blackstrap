#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <string.h>
#include <time.h>
#include <errno.h>

#include <blackstrap-gpio.h>
#include <blackstrap-log.h>

#define UNEXPORT (0)
#define EXPORT (1)

ssize_t get_gpio_file_str(int pin, const char *file, char *buf);
int set_gpio_file_str(int pin, const char *file, const char *value);
int get_gpio_file_int(int pin, const char *file, int *value);
ssize_t set_gpio_file_int(int pin, const char *file, int value);

ssize_t read_file_str(const char *filename, char *buf, size_t len);
int write_file_str(const char *filename, const char *str, size_t len);
int read_file_int(const char *filename, int *value);
ssize_t write_file_int(const char *filename, int value);

int export_unexport(int pin, int export);
int gpio_wait_stable(int pin);
void blackstrap_gpio_build_filename(char *buf, int pin, const char *file);


int
blackstrap_gpio_export(int pin) {
    if (!blackstrap_gpio_exported(pin)) {
	if (export_unexport(pin, EXPORT))
	    return -1;
    }
    if (gpio_wait_stable(pin) < 0)
	return -1;
    if (blackstrap_gpio_set_direction(pin, BLACKSTRAP_GPIO_DIRECTION_IN) < 0)
	return -1;
    if (blackstrap_gpio_set_edge(pin, BLACKSTRAP_GPIO_EDGE_NONE) < 0)
	return -1;
    return blackstrap_gpio_set_active_low(pin,
	    BLACKSTRAP_GPIO_ACTIVE_LOW_DISABLE);
}

int
blackstrap_gpio_unexport(int pin) {
    if (!blackstrap_gpio_exported(pin))
	return 0;
    if (export_unexport(pin, UNEXPORT) < 0)
	return -1;

    struct timespec sleep_time = { 0, 1000000 }; // 1 ms

    int gone = BLACKSTRAP_GPIO_EXPORT_MAX_WAIT_MS;
    for (; gone; gone--) {
	if (!blackstrap_gpio_exported(pin))
	    break;
	nanosleep(&sleep_time, NULL);
    }
    if (!gone)
	return -1;

    return 0;
}

int blackstrap_gpio_exported(int pin) {
    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    snprintf(filename, BLACKSTRAP_GPIO_FILENAME_MAX_LEN-1,
	    "/sys/class/gpio/gpio%d", pin);
    return !access(filename, W_OK);
}

ssize_t
blackstrap_gpio_get_direction(int pin, char *direction) {
    return get_gpio_file_str(pin, "direction", direction);
}

int
blackstrap_gpio_set_direction(int pin, const char *direction) {
    return set_gpio_file_str(pin, "direction", direction);
}

ssize_t
blackstrap_gpio_get_edge(int pin, char *edge) {
    return get_gpio_file_str(pin, "edge", edge);
}

int
blackstrap_gpio_set_edge(int pin, const char *edge) {
    return set_gpio_file_str(pin, "edge", edge);
}

int
blackstrap_gpio_get_value(int pin, int *value) {
    return get_gpio_file_int(pin, "value", value);
}

ssize_t
blackstrap_gpio_set_value(int pin, int value) {
    return set_gpio_file_int(pin, "value", value ? 1 : 0);
}

ssize_t
blackstrap_gpio_get_active_low(int pin, int *active_low) {
    return get_gpio_file_int(pin, "active_low", active_low);
}

int
blackstrap_gpio_set_active_low(int pin, int active_low) {
    return set_gpio_file_int(pin, "active_low", active_low ? 1 : 0);
}

// - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -

int
export_unexport(int pin, int export) {
    char export_filename[]   = "/sys/class/gpio/export";
    char unexport_filename[] = "/sys/class/gpio/unexport";

    char *filename = export ? export_filename : unexport_filename;

    int fd = open(filename, O_WRONLY|O_TRUNC|O_SYNC);
    if (fd < 0)
	return fd;
    if (dprintf(fd, "%d", pin) < 0)
	return -1;
    return close(fd);
}

int gpio_wait_stable(int pin) {
    const char *files[] = {
	"value",
	"edge",
	"direction",
	"active_low"
    };
    int files_len = (int)(sizeof files / sizeof (const char *));

    struct timespec sleep_time = { 0, 1000000 }; // 1 ms
    int ready = BLACKSTRAP_GPIO_EXPORT_MAX_WAIT_MS;

    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    for (int file_num=0; file_num < files_len; file_num++) {
	blackstrap_gpio_build_filename(filename, pin, files[file_num]);
	for (; ready; ready--) {
	    if (access(filename, W_OK) == 0)
		break;
	    nanosleep(&sleep_time, NULL);
	}
    }
    if (!ready)
	return -1;

    return 0;
}

int
get_gpio_file_int(int pin, const char *file, int *value) {
    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    blackstrap_gpio_build_filename(filename, pin, file);

    return read_file_int(filename, value);
}

ssize_t
set_gpio_file_int(int pin, const char *file, int value) {
    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    blackstrap_gpio_build_filename(filename, pin, file);
    return write_file_int(filename, value);
}

ssize_t
get_gpio_file_str(int pin, const char *file, char *buf) {
    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    blackstrap_gpio_build_filename(filename, pin, file);

    return read_file_str(filename, buf, BLACKSTRAP_FILE_VALUE_MAX_LEN);
}

int
set_gpio_file_str(int pin, const char *file, const char *value) {
    char filename[BLACKSTRAP_GPIO_FILENAME_MAX_LEN];
    blackstrap_gpio_build_filename(filename, pin, file);

    return write_file_str(filename, value, strlen(value));
}

int
read_file_int(const char *filename, int *value) {
    char buf[32];
    ssize_t read_len = read_file_str(filename, buf, 32);
    if (read_len < -1)
	return -1;

    char *endptr = NULL;
    long int val = strtol(buf, &endptr, 10);
    if (!*endptr)
       return -1;

    *value = (int)val;
    return 0;
}

ssize_t
write_file_int(const char *filename, int value) {
    int fd = open(filename, O_WRONLY|O_TRUNC);
    if (fd < 0)
	return fd;

    ssize_t write_len = dprintf(fd, "%d", value);
    if (write_len < 0) {
	int old_errno = errno;
	close(fd);
	errno = old_errno;
	return write_len;
    }

    int err = close(fd);
    if (err < 0)
	return err;

    return write_len;
}

ssize_t
read_file_str(const char *filename, char *buf, size_t len) {
    int fd = open(filename, O_RDONLY);
    if (fd < 0)
	return fd;

    if (len > BLACKSTRAP_FILE_VALUE_MAX_LEN)
	return -1;

    ssize_t read_len = read(fd, buf, len);

    if (read_len < 0) {
	int old_errno = errno;
	close(fd);
	errno = old_errno;
	return read_len;
    }

    int err = close(fd);
    if (err < 0)
	return err;

    return read_len;
}

int
write_file_str(const char *filename, const char *str,
	size_t len) {
    int fd = open(filename, O_WRONLY);
    if (fd < 0) {
	blackstrap_log_error("open failed for %s: %s", filename,
		strerror(errno));
	return fd;
    }
    if (write(fd, str, len) != (ssize_t)len) {
	blackstrap_log_error("write failed for %s", filename);
        return -1;
    }
    return close(fd);
}

void
blackstrap_gpio_build_filename(char *buf, int pin, const char *file) {
    snprintf(buf, BLACKSTRAP_GPIO_FILENAME_MAX_LEN-1,
	    "/sys/class/gpio/gpio%d/%s", pin, file);
}

