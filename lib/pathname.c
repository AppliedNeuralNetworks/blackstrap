
#include <stdlib.h>
#include <limits.h>
#include <string.h>
#include "pathname.h"

/**
 * Get the directory name of a path.
 * Returns an allocated string buffer. Free with free().
 */
char *dir_name(const char *path) {
    register char *pathc;
    size_t len;
    register char *p;

    if (path == NULL)				// NULL is out of band
       return NULL;

    len = strnlen(path, PATH_MAX);
    if (len == PATH_MAX)			// path too long?
	return NULL;

    if (!len)					// was: empty, dir is "."
	return strdup(".");

    pathc = strdup(path);			// copy for pointer math

    p = pathc + len - 1;
    while (p >= pathc && *p == '/') --p;	// walk trailing slashes
    if (p < pathc) {
	free(pathc);
	return strdup("/");			// was: slash, or all slashes
    }
	
    while (p >= pathc && *p != '/') --p;	// walk non-slashes
    if (p < pathc) {
	free(pathc);
	return strdup(".");			// was: single relative item
    }
	
    while (p >= pathc && *p == '/') --p;	// walk intervening slashes
    if (p < pathc) {
	free(pathc);
	return strdup("/");			// was: item in root
    }

    *(p+1) = 0;					// set null past position
    return pathc;
}

/**
 * Get the base name of a path.
 * Returns an allocated string buffer. Free with free().
 */
char *base_name(const char *path) {
    register char *pathc;
    size_t len;
    register char *p, *s;

    if (path == NULL)				// NULL is out of band
       return NULL;

    len = strnlen(path, PATH_MAX);
    if (len == PATH_MAX)			// path too long?
	return NULL;

    if (!len)					// was: empty, no basename
	return strdup("");

    pathc = strdup(path);

    p = pathc + len - 1;
    while (p >= pathc && *p == '/') --p;	// walk trailing slashes
    if (p < pathc) {
	free(pathc);
	return strdup("/");			// was: slash, or all slashes
    }
    *(p+1) = 0;					// set null past position
	
    while (p >= pathc && *p != '/') --p;	// walk non-slashes
    if (p < pathc) {
	return pathc;				// was: single relative item
    }
	
    s = pathc;					// move forward in buffer
    while(*p++)
	*s++ = *p;

    return pathc;
}

char *parent_dir_name(const char *path) {
    char *dir;
    char *parent_dir;

    dir = dir_name(path);
    if (!dir)
	return NULL;
    parent_dir = dir_name(dir);
    free(dir);
    return parent_dir;
}

char *parent_parent_dir_name(const char *path) {
    char *parent_dir;
    char *parent_parent_dir;

    parent_dir = parent_dir_name(path);
    if (!parent_dir)
	return NULL;
    parent_parent_dir = dir_name(parent_dir);
    free(parent_dir);
    return parent_parent_dir;
}

char *parent_base_name(const char *path) {
    char *dir;
    char *parent_base;

    dir = dir_name(path);
    if (!dir)
	return NULL;
    parent_base = base_name(dir);
    free(dir);

    return parent_base;
}

char *parent_parent_base_name(const char *path) {
    char *dir;
    char *parent_dir;
    char *parent_base;

    dir = dir_name(path);
    if (!dir)
	return NULL;
    parent_dir = dir_name(dir);
    if (!parent_dir)
	return NULL;
    parent_base = base_name(parent_dir);
    free(parent_dir);
    free(dir);

    return parent_base;
}

int base_name_is(const char *path, const char *str) {
    char *base;
    int result;

    base = base_name(path);
    if (!base)
	return 0;
    result = !strcmp(base, str);
    free(base);

    return result;
}

int parent_base_name_is(const char *path, const char *str) {
    char *parent_base;
    int result;

    parent_base = parent_base_name(path);
    if (!parent_base)
	return 0;
    result = !strcmp(parent_base, str);
    free(parent_base);

    return result;
}

int parent_parent_base_name_is(const char *path, const char *str) {
    char *parent_parent_base;
    int result;

    parent_parent_base = parent_parent_base_name(path);
    if (!parent_parent_base)
	return 0;
    result = !strcmp(parent_parent_base, str);
    free(parent_parent_base);

    return result;
}

int dir_name_is(const char *path, const char *str) {
    char *dir;
    int result;

    dir = dir_name(path);
    if (!dir)
	return 0;
    result = !strcmp(dir, str);
    free(dir);

    return result;
}

int parent_dir_name_is(const char *path, const char *str) {
    char *parent_dir;
    int result;

    parent_dir = parent_dir_name(path);
    if (!parent_dir)
	return 0;
    result = !strcmp(parent_dir, str);
    free(parent_dir);

    return result;
}

int parent_parent_dir_name_is(const char *path, const char *str) {
    char *parent_parent_dir;
    int result;

    parent_parent_dir = parent_parent_dir_name(path);
    if (!parent_parent_dir)
	return 0;
    result = !strcmp(parent_parent_dir, str);
    free(parent_parent_dir);

    return result;
}

