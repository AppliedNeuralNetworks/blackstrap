
#include <errno.h>

#include "blackstrap.h"

zsock_t *
blackstrap_i2cd_connect(const char *endpoint) {
    zsock_t *sock = zsock_new(ZMQ_REQ);
    if (!sock)
	return NULL;
    int err = zsock_connect(sock, "%s", endpoint);
    if (err < 0)
	return NULL;

    return sock;
}

int
blackstrap_i2cd_noop(zsock_t *sock, struct timespec *timestamp) {
    blackstrap_send_action(sock, I2CD_ACTION_NOOP, ZFRAME_FINAL);
    zmsg_t *reply = zmsg_recv(sock);

    int status = blackstrap_msg_shift_status(reply);
    if (timestamp)
	blackstrap_msg_shift_time(reply, timestamp);
    zmsg_destroy(&reply);

    return status;
}

int
blackstrap_i2cd_read(zsock_t *sock, uint8_t bus, uint8_t address,
	uint8_t command, uint8_t *buf, uint8_t length,
	struct timespec *timestamp) {
    blackstrap_send_action(sock, I2CD_ACTION_READ, ZFRAME_MORE);
    blackstrap_send_byte(sock, bus,     ZFRAME_MORE);
    blackstrap_send_byte(sock, address, ZFRAME_MORE);
    blackstrap_send_byte(sock, command, ZFRAME_MORE);
    blackstrap_send_byte(sock, length,  ZFRAME_FINAL);

    zmsg_t *reply = zmsg_recv(sock);
    int status = blackstrap_msg_shift_status(reply);
    if (status >= 0) {
	if (timestamp)
	    blackstrap_msg_shift_time(reply, timestamp);
	else
	    blackstrap_msg_shift(reply);

	if (buf)
	    blackstrap_msg_shift_bytes(reply, buf);
	else {
	    blackstrap_log_error("read: buffer pointer was null\n");
	    blackstrap_msg_shift(reply);
	}
    }
    zmsg_destroy(&reply);

    return status;
}

uint8_t
blackstrap_i2cd_read_byte(zsock_t *sock, uint8_t bus, uint8_t address,
       	uint8_t command, struct timespec *timestamp) {
    blackstrap_send_action(sock, I2CD_ACTION_READ_BYTE, ZFRAME_MORE);
    blackstrap_send_byte(sock, bus,     ZFRAME_MORE);
    blackstrap_send_byte(sock, address, ZFRAME_MORE);
    blackstrap_send_byte(sock, command, ZFRAME_FINAL);

    zmsg_t *reply = zmsg_recv(sock);
    int status = blackstrap_msg_shift_status(reply);
    if (status < 0) {
	zmsg_destroy(&reply);
	errno = EINVAL;
	return 0;
    }
    if (timestamp)
	blackstrap_msg_shift_time(reply, timestamp);
    else
	blackstrap_msg_shift(reply);

    uint8_t data = blackstrap_msg_shift_byte(reply);
    zmsg_destroy(&reply);

    return data;
}

int
blackstrap_i2cd_write_byte(zsock_t *sock, uint8_t bus, uint8_t address,
       	uint8_t command, uint8_t value) {
    blackstrap_send_action(sock, I2CD_ACTION_WRITE_BYTE, ZFRAME_MORE);
    blackstrap_send_byte(sock, bus,     ZFRAME_MORE);
    blackstrap_send_byte(sock, address, ZFRAME_MORE);
    blackstrap_send_byte(sock, command, ZFRAME_MORE);
    blackstrap_send_byte(sock, value,   ZFRAME_FINAL);

    return blackstrap_recv_status(sock);
}


int
blackstrap_i2cd_write(zsock_t *sock, uint8_t bus, uint8_t address,
	uint8_t command, uint8_t *data, uint8_t length) {
    blackstrap_send_action(sock, I2CD_ACTION_WRITE, ZFRAME_MORE);
    blackstrap_send_byte(sock, bus, ZFRAME_MORE);
    blackstrap_send_byte(sock, address, ZFRAME_MORE);
    blackstrap_send_byte(sock, command, ZFRAME_MORE);
    blackstrap_send_bytes(sock, data, length, ZFRAME_FINAL);

    return blackstrap_recv_status(sock);
}

