
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <stdio.h>
#include <stdarg.h>
#include <czmq.h>

#include "blackstrap-pathlist.h"


blackstrap_pathlist_t *
blackstrap_pathlist_new(void) {
    blackstrap_pathlist_t *pathlist = zlist_new();
    zlist_autofree(pathlist);
    return pathlist;
}

void
blackstrap_pathlist_destroy(blackstrap_pathlist_t **pathlist) {
    zlist_destroy(pathlist);
}

__attribute__((__format__(__printf__, 2, 0)))
int
blackstrap_pathlist_append(blackstrap_pathlist_t *pathlist,
       	const char *format, ...) {
    int result;
    char *path;
    va_list argptr;

    va_start(argptr, format);
    result = vasprintf(&path, format, argptr);
    va_end(argptr);
    if (result < 0)
	return result;

    result = zlist_append(pathlist, (void *)path);
    free(path);

    return result;
}

__attribute__((__format__(__printf__, 2, 0)))
int
blackstrap_pathlist_prepend(blackstrap_pathlist_t *pathlist,
       	const char *format, ...) {
    int result;
    char *path;
    va_list argptr;

    va_start(argptr, format);
    result = vasprintf(&path, format, argptr);
    va_end(argptr);
    if (result < 0)
	return result;

    result = zlist_push(pathlist, (void *)path);
    free(path);

    return result;
}

const char *
blackstrap_pathlist_first_existing(blackstrap_pathlist_t *pathlist) {
    zlist_first(pathlist);
    const char *path;
    while ((path = zlist_item(pathlist))) {
	if (zsys_file_exists(path))
	    return path;
	zlist_next(pathlist);
    }
    return NULL;
}

