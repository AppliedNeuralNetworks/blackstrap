
#include <string.h>
#include <stdio.h>
#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <stdlib.h>
#include <time.h>

#include "blackstrap-misc.h"

char *
strrstr(const char *haystack, const char *needle) {
    if (!haystack || !needle)
        return NULL;

    size_t hsize = strlen(haystack),
           nsize = strlen(needle);
    if (nsize > hsize)
        return NULL;

    // Safely copy haystack to char * without casting away constness
    union {
        const char *const_charp;
        char *charp;
    } safe_charp;
    safe_charp.const_charp = haystack;
    char *hstk = safe_charp.charp;

    char *pos = hstk + hsize - nsize;
    for (; pos >= hstk; pos--)
        if (!strncmp(pos, needle, nsize))
            return pos;

    return NULL;
}

int
get_long_from_file(long *value, const char *filename) {
    int fd = open(filename, O_RDONLY);
    if (fd < 0)
	return -1;

    char buf[16];
    if (read(fd, buf, 16) < 1) {
	close(fd);
	return -1;
    }

    if (close(fd) < 0)
	return -1;

    errno = 0;
    *value = strtol(buf, NULL, 10);
    if (errno)
	return -1;

    return 0;
}

char *
format_time_8601plus(struct timespec *timestamp) {
    struct tm *ltime = localtime(&timestamp->tv_sec);
    static char buf[32];

    strftime(buf, 20, "%F %T", ltime);
    snprintf(buf + strlen(buf), 11, ".%09li", timestamp->tv_nsec);

    return buf;
}

