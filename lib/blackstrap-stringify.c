
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <stdio.h>
#include <stdlib.h>

#include "blackstrap-stringify.h"

#ifndef STRINGIFY_FORMAT
#    define STRINGIFY_FORMAT "%ud, %02xh, %sb"
#endif

char *
blackstrap_stringify_uchar_bin(unsigned char value) {
    char *string = malloc(9);
    string[8] = 0;
    for(int i = 7 ; i >= 0; i--)
	string[7 - i] = (value & (1 << i)) ? '1' : '0';

    return string;
}

char *
blackstrap_stringify_uchar(unsigned char value) {
    char *bin_str = blackstrap_stringify_uchar_bin(value);
    if (!bin_str)
	return NULL;

    char *string = NULL;
    int result = asprintf(&string, STRINGIFY_FORMAT, value, value, bin_str);
    free(bin_str);
    if (result < 0)
	return NULL;

    return string;
}

char *
blackstrap_stringify_msg_uchar(const char *message, unsigned char value) {
    char *bin_str = blackstrap_stringify_uchar_bin(value);
    if (!bin_str)
	return NULL;

    char *string = NULL;
    int result = 0;
    if (message)
	result = asprintf(&string, "%s: " STRINGIFY_FORMAT,
	       	message, value, value, bin_str);
    else
	result = asprintf(&string, STRINGIFY_FORMAT, value, value, bin_str);
    free(bin_str);
    if (result < 0)
	return NULL;

    return string;
}
