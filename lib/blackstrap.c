
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include "blackstrap.h"

static zconfig_t *appconf[BLACKSTRAP_APPCONF_MAX];

void blackstrap_appconf_init(void);
void blackstrap_appconf_cleanup(void);
void blackstrap_appconf_build_pathinfo(void);
void blackstrap_appconf_load_system_conf(void);
void blackstrap_appconf_load_project_conf(void);
char* find_project_dir(void);


//---------------------------------------------- Initialization and Termination

void
blackstrap_init(void) {
    blackstrap_appconf_build_pathinfo();
    blackstrap_log_init();		// Temporary default logging
    blackstrap_appconf_init();
    blackstrap_log_cleanup();		// Restart logs with new configuration
    blackstrap_log_init();
}

void
blackstrap_cleanup(void) {
    blackstrap_appconf_cleanup();
    blackstrap_log_cleanup();
}

//------------------------------------------------------------ Path Information

char *
find_project_dir(void) {
    const char *app_dir = blackstrap_appconf_resolve("pathinfo/app_dir", NULL);
    if (!app_dir)
        return NULL;

    size_t len = strlen(app_dir);
    if (len <= 1)			// empty string or "/"
	return NULL;

    char *dir = malloc(len + 6);	// space for /conf{null}
    strcpy(dir, app_dir);

    char *pos = dir + len;		// at trailing null byte
    for (int i = 0; i <= 2; i++) {	// only search this dir plus two levels
	strcat(dir, "/conf");		// add suffix and test
        if (!access(dir, R_OK)) {
	    *pos = 0;			// found; remove suffix
            return dir;
	}

	pos -= 5;			// jump suffix
	while (*--pos != '/');		// walk directory node
	if (pos <= dir)			// string exhausted
	    break;
	*pos = 0;			// truncate
    }
    free(dir);				// not found

    return NULL;
}

void
blackstrap_appconf_build_pathinfo(void) {
    zconfig_t *pathinfo = zconfig_new("root", NULL);
    blackstrap_appconf_level_set(pathinfo, BLACKSTRAP_APPCONF_PATHINFO);

    // App realpath
    char *app_realpath = realpath(program_invocation_name, NULL);
    zconfig_put(pathinfo, "pathinfo/app_realpath", app_realpath);

    // App directory
    char *app_dir = dir_name(app_realpath);
    free(app_realpath);
    zconfig_put(pathinfo, "pathinfo/app_dir", app_dir);
    free(app_dir);

    // Project directory, if present
    char *project_dir = find_project_dir();
    if (project_dir) {
	zconfig_put(pathinfo, "pathinfo/project_dir", project_dir);
	free(project_dir);
    }

    // TODO: User config directory
}

//--------------------------------------------------- Application Configuration

void
blackstrap_appconf_load_specific_conf(void) {
    const char *filename = blackstrap_appconf_resolve(
	    "configuration/filename", NULL);
    if (!filename)
	return;

    zconfig_t *conf = blackstrap_conf_load(filename);
    if (!conf)
	blackstrap_log_error_exit("Configuration specified on the command "
		"line could not be loaded");

    blackstrap_appconf_level_set(conf, BLACKSTRAP_APPCONF_SPECIFIC);
}

void
blackstrap_appconf_load_project_conf(void) {
    const char *project_dir = blackstrap_appconf_resolve(
	    "pathinfo/project_dir", NULL);
    if (!project_dir)
	return;

    blackstrap_pathlist_t *pathlist = blackstrap_pathlist_new();
    blackstrap_pathlist_append(pathlist, "%s/conf/services/%s.conf",
	    project_dir, program_invocation_short_name);
    blackstrap_pathlist_append(pathlist, "%s/conf/%s.conf",
	    project_dir, program_invocation_short_name);

    const char *filename = blackstrap_pathlist_first_existing(pathlist);

    if (!filename) {
	blackstrap_pathlist_destroy(&pathlist);
	return;
    }

    zconfig_t *conf = blackstrap_conf_load(filename);

    if (!conf) {
	blackstrap_log_error("Project-local application configuration "
		"\"%s\" was found but could not be loaded", filename);
	blackstrap_pathlist_destroy(&pathlist);
	exit(EXIT_FAILURE);
    }

    blackstrap_appconf_level_set(conf, BLACKSTRAP_APPCONF_PACKAGE);
    blackstrap_pathlist_destroy(&pathlist);
}

void
blackstrap_appconf_load_system_conf(void) {
    blackstrap_pathlist_t *pathlist = blackstrap_pathlist_new();
    blackstrap_pathlist_append(pathlist, "/srv/project/blackstrap/"
	    "services/%s.conf", program_invocation_short_name);
    blackstrap_pathlist_append(pathlist, "/srv/project/blackstrap/"
	    "conf/%s.conf", program_invocation_short_name);
    blackstrap_pathlist_append(pathlist, "/etc/blackstrap/services/%s.conf",
	    program_invocation_short_name);
    blackstrap_pathlist_append(pathlist, "/etc/blackstrap/%s.conf",
	    program_invocation_short_name);
    blackstrap_pathlist_append(pathlist, "/etc/%s/%s.conf",
	    program_invocation_short_name, program_invocation_short_name);

    const char *filename = blackstrap_pathlist_first_existing(pathlist);
    if (!filename) {
	blackstrap_pathlist_destroy(&pathlist);
	return;
    }

    zconfig_t *conf = blackstrap_conf_load(filename);
    if (!conf) {
	blackstrap_log_error("System-level application configuration "
		"\"%s\" was found but could not be loaded", filename);
	blackstrap_pathlist_destroy(&pathlist);
	exit(EXIT_FAILURE);
    }

    blackstrap_appconf_level_set(conf, BLACKSTRAP_APPCONF_SYSTEM);
    blackstrap_pathlist_destroy(&pathlist);
}

void
blackstrap_appconf_init(void) {
    blackstrap_appconf_load_specific_conf();
    // TODO: blackstrap_appconf_load_user_conf();
    blackstrap_appconf_load_project_conf();
    blackstrap_appconf_load_system_conf();
}

void blackstrap_appconf_cleanup(void) {
    for (int level = 0; level < BLACKSTRAP_APPCONF_MAX; level++)
	if (appconf[level])
	    zconfig_destroy(&appconf[level]);
}

const char *
blackstrap_appconf_resolve(const char *path, const char *default_value) {
    zconfig_t *item = NULL;
    blackstrap_appconf_level level;
    for (level = 0; level < BLACKSTRAP_APPCONF_MAX; level++) {
	if (!appconf[level])
	    continue;
	// TODO: Move !item to for() continuation conditions
	if ((item = zconfig_locate(appconf[level], path)))
	    break;
    }
    if (!item)
	return default_value;

    return zconfig_value(item);
}

zconfig_t *
blackstrap_appconf_level_get(blackstrap_appconf_level level) {
    return appconf[level];
}

void
blackstrap_appconf_level_set(zconfig_t *conf,
	blackstrap_appconf_level level) {
    if (appconf[level]) {
	blackstrap_log_warning("Replacing configuration level %d", level);
	zconfig_destroy(&appconf[level]);
    }
    appconf[level] = conf;
}


