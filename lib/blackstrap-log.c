
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

#include "blackstrap-stringify.h"
#include "blackstrap-log.h"

static int log_initialized;


void
blackstrap_log_init(void) {
    if (log_initialized)
	return;
    openlog(NULL, LOG_PERROR | LOG_CONS | LOG_PID, LOG_DAEMON);
    log_initialized = 1;
}

void
blackstrap_log_cleanup(void) {
    closelog();
    log_initialized = 0;
}

__attribute__((__format__(__printf__, 2, 0)))
void
blackstrap_vlog(int priority, const char *format, va_list argptr) {
    if (!log_initialized)
	blackstrap_log_init();
    char *string;
    vasprintf(&string, format, argptr);
    syslog(priority, "%s", string);
    free(string);
}

__attribute__((__format__(__printf__, 1, 0)))
void
blackstrap_log_error(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_ERR, format, argptr);
    va_end(argptr);
}

__attribute__((noreturn, __format__(__printf__, 1, 0)))
void
blackstrap_log_error_exit(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_ERR, format, argptr);
    va_end(argptr);
    exit(EXIT_FAILURE);
}

__attribute__((__format__(__printf__, 2, 0)))
void
blackstrap_do_or_die(int status, const char *format, ...) {
    if (status >= 0)
	return;
    if (!log_initialized)
	blackstrap_log_init();
    if (format) {
	va_list argptr;
	va_start(argptr, format);
	blackstrap_vlog(LOG_ERR, format, argptr);
	va_end(argptr);
    }
    exit(EXIT_FAILURE);
}

__attribute__((noreturn, __format__(__printf__, 1, 0)))
void
blackstrap_log_error_abort(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_ERR, format, argptr);
    va_end(argptr);
    abort();
}

__attribute__((__format__(__printf__, 1, 0)))
void
blackstrap_log_warning(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_WARNING, format, argptr);
    va_end(argptr);
}

__attribute__((__format__(__printf__, 1, 0)))
void
blackstrap_log_notice(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_NOTICE, format, argptr);
    va_end(argptr);
}

__attribute__((__format__(__printf__, 1, 0)))
void
blackstrap_log_info(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_INFO, format, argptr);
    va_end(argptr);
}

__attribute__((__format__(__printf__, 1, 0)))
void
blackstrap_log_debug(const char *format, ...) {
    if (!log_initialized)
	blackstrap_log_init();
    va_list argptr;
    va_start(argptr, format);
    blackstrap_vlog(LOG_DEBUG, format, argptr);
    va_end(argptr);
}

void
blackstrap_log_uchar_msg_value(const char *message, unsigned char value) {
    char *string = blackstrap_stringify_msg_uchar(message, value);
    if (!string)
	return;
    blackstrap_log_debug("%s", string);
    free(string);
}

