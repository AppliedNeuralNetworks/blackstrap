
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wunknown-pragmas"
#pragma clang diagnostic ignored "-Wreserved-id-macro"
#define _GNU_SOURCE
#pragma clang diagnostic pop

#include <czmq.h>

#include "blackstrap-log.h"
#include "lockfile.h"

static char *lockfile_name;
static int lockfile_fd = -1;
static struct flock lock;

int lockfile_cache_name(void);


int
lockfile_acquire(void) {
    char buf[16];

    if (lockfile_cache_name() < 0)
	    return -1;

    if (lockfile_fd != -1) {
	blackstrap_log_error("Lockfile already locked by this process: %s",
	       	lockfile_name);
	return 0;
    }

    int fd = open(lockfile_name, O_WRONLY | O_CREAT | O_TRUNC | O_SYNC, 0644);
    if (fd < 0) {
	blackstrap_log_error("Could not open lock file %s: %s", lockfile_name,
		strerror(errno));
	return -1;
    }

    sprintf(buf, "%d\n", getpid());
    write(fd, buf, strlen(buf));

    lock.l_type = F_WRLCK;
    if (fcntl(fd, F_SETLK, &lock) < 0) {
	blackstrap_log_error("Could not establish lock %s: %s", lockfile_name,
		strerror(errno));
	return -1;
    }

    lockfile_fd = fd;

    blackstrap_log_info("Acquired exclusive lock");
    return 0;
}

int
lockfile_release(void) {
    if (lockfile_fd == -1) {
	blackstrap_log_error("Lockfile not locked by this process: %s",
	       	lockfile_name);
	return -1;
    }

    lock.l_type = F_UNLCK;
    if (fcntl(lockfile_fd, F_SETLK, &lock) < 0) {
	blackstrap_log_error("Unable to unlock lockfile: %s", lockfile_name);
	return -1;
    }

    close(lockfile_fd);
    lockfile_fd = -1;

    blackstrap_log_info("Removing lockfile");
    remove(lockfile_name);
    zstr_free(&lockfile_name);

    return 0;
}

int
lockfile_kill_process(void) {
    if (lockfile_cache_name() < 0)
	    return -1;

    errno = 0;
    int fd = open(lockfile_name, O_RDWR);
    if (fd < 0) {
	if (errno == ENOENT)
	    return 0;
    	else
	    return -1;
    }

    lock.l_type = F_WRLCK;
    int err = fcntl(fd, F_GETLK, &lock);
    close(fd);
    if (err < 0) {
	blackstrap_log_error("Unable to get lock status");
	return -1;
    }
    if (lock.l_type != F_WRLCK) {
	blackstrap_log_warning("Removing stale lockfile");
	remove(lockfile_name);
	zstr_free(&lockfile_name);
	return 0;
    }

    if (lock.l_pid == getpid()) {
	blackstrap_log_error("Attempted to terminate own process");
	return -1;
    }

    if (kill(lock.l_pid, 15) < 0) {
	blackstrap_log_error("Could not terminate PID %d: %s", lock.l_pid,
	       	strerror(errno));
	return -1;
    }
    blackstrap_log_info("Terminated PID %d", lock.l_pid);

    for (int msec = 1; msec < 1000; msec *= 2) {
	if (!zsys_file_exists(lockfile_name)) {
	    zstr_free(&lockfile_name);
	    return 0;
	}
	zclock_sleep(msec);
    }

    blackstrap_log_error("Timeout waiting for process to exit");
    zstr_free(&lockfile_name);

    return -1;
}

int
lockfile_cache_name(void) {
    if (lockfile_name)
	return 0;
    if (asprintf(&lockfile_name, LOCKFILE_PATH "%s" LOCKFILE_SUFFIX,
		program_invocation_short_name) < 0) {
	blackstrap_log_error("Could not build lockfile name");
	return -1;
    }
    return 0;
}

