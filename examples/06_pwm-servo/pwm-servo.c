/*
 * This program will sweep a servo attached to channel 0 of a
 * PCA9685.
 */

#include <math.h>
#include <blackstrap.h>

// The listening port of running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

// The I2C bus on the destination host. For example, the Raspberry Pi
// Model B provides a connection to I2C bus 1 on the large header block.
#define BUS (1)

#define SERVO_MIN  (200)
#define SERVO_MAX  (500)
#define SERVO_REPS (2)
#define SERVO_DELAY (1000)


int delay(int ticks);
void wake_mode(void);
void sleep_mode(void);
void restart(void);
void set_frequency(uint16_t freq);
void set_pwm(uint16_t on);

static zconfig_t *conf;
static zsock_t *sock;

int
main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library.)
    blackstrap_init();

    // Load the device configuration
    conf = blackstrap_i2cd_field_load("pca9685");
    if (!conf)
	blackstrap_log_error_exit("Unable to load conf");

    // Connect to i2cd
    sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
	blackstrap_log_error_exit("Unable to connect to i2cd");

    // Restart the PCA9685 and set the mode registers
    restart();

    // Set the prequency to 50Hz (about 20ms per cycle)
    set_frequency(50);

    // Pulse always begins at tick 0
    blackstrap_i2cd_field_set(sock, conf, BUS, "led0_on_h", 0);
    blackstrap_i2cd_field_set(sock, conf, BUS, "led0_on_l", 0);

    for (int i = SERVO_REPS; i; i--) {
	set_pwm(SERVO_MIN);
	if (delay(SERVO_DELAY))
	    break;
	set_pwm(SERVO_MAX);
	if (delay(SERVO_DELAY))
	    break;
    }

    // Set the servo back to the middle of its sweep range
    set_pwm(SERVO_MIN + (SERVO_MAX - SERVO_MIN)/2);

    // Destroy the socket
    zsock_destroy(&sock);

    // Unload the device configuration. This must be done explicitely,
    // since it was loaded specifically here.
    blackstrap_i2cd_field_unload(&conf);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

void wake_mode(void) {
    int mode1 = blackstrap_i2cd_field_get(sock, conf, BUS, "mode_1", NULL);
    // Mask restart mode and sleep mode
    blackstrap_i2cd_field_set(sock, conf, BUS, "mode_1",
	    (uint8_t)mode1 & 0x6f);
    delay(1);
}

void sleep_mode(void) {
    int mode1 = blackstrap_i2cd_field_get(sock, conf, BUS, "mode_1", NULL);
    // Mask restart mode, but set sleep mode
    blackstrap_i2cd_field_set(sock, conf, BUS, "mode_1",
	    (uint8_t)(mode1 & 0x7f) | 0x10);
    delay(1);
}

void restart(void) {
    wake_mode();
    sleep_mode();
    // Set restart, auto-increment, and respond to "all call" address
    blackstrap_i2cd_field_set(sock, conf, BUS, "mode_1", 0xa1);
    // ...will restart here. Wait at least 500 microseconds
    delay(1);
    // Enable totem-pole output drive
    blackstrap_i2cd_field_set(sock, conf, BUS, "mode_2", 0x04);
}

int delay(int ticks) {
    static struct timespec sleep_time_1ms = {
       	.tv_sec = 0, .tv_nsec = 1000000 };
    for (int j = ticks; j; j--)
	if (nanosleep(&sleep_time_1ms, NULL)) // if sleep was interrupted
	    return -1;
    return 0;
}

void set_pwm(uint16_t ticks) {
    blackstrap_i2cd_field_set(sock, conf, BUS,
	    "led0_off_h", (ticks & 0xff00) >> 8);
    blackstrap_i2cd_field_set(sock, conf, BUS,
	    "led0_off_l", ticks & 0xff);
}

void set_frequency(uint16_t hz) {
    sleep_mode();
    blackstrap_i2cd_field_set(sock, conf, BUS, "pre_scale",
	    (uint8_t)(round(25000000 / (4096 * hz)) - 1));
    wake_mode();
}

