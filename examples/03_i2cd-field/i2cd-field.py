#!/usr/bin/env python3

import sys
from os.path import realpath, dirname, join

# Add project Python library to the import path
script_dir = dirname(sys.argv[0])
lib_dir = join(script_dir, '..', '..', 'pyblackstrap', 'lib')
sys.path.insert(0, realpath(lib_dir))

import blackstrap

# The listening port of running i2cd with which to make a connection.
endpoint = "tcp://127.0.0.1:1255"

# The I2C bus on the destination host. For example, the Raspberry Pi
# Model B provides a connection to I2C bus 1 on the large header block.
bus = 1

# The name of the register to get/set
field_name = 'output_data_rate'

# Create a ZMQ socket and connect to i2cd's listening port.
i2cd = blackstrap.I2Cd(endpoint)

# Load a field configuration file for this device
i2cd_field = blackstrap.I2CdField("lsm303dlhc-accelerometer")

# Set the numeric value of a named field
i2cd_field.set(i2cd, bus, field_name, 0x02) # value for 10_Hz

# Get the numeric value of a named field
timestamp, value = i2cd_field.get(i2cd, bus, field_name)

print("At time %s, the value of register %s was 0x%02x" %
      (timestamp, field_name, value))

# Set named field to a value by description
i2cd_field.set_str(i2cd, bus, field_name, '25_Hz')

# Get a descriptive value of a named field
value_descr, timestamp = i2cd_field.get_str(i2cd, bus, field_name)

print("At time %s, the value of field %s was %s" %
      (timestamp, field_name, value_descr))

