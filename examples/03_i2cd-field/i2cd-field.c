
#include <blackstrap.h>

// The listening port of running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

// The I2C bus on the destination host. For example, the Raspberry Pi
// Model B provides a connection to I2C bus 1 on the large header block.
#define BUS (1)

// The desired field, as listed in the device configuration file.
#define FIELD "output_data_rate"

// A textual description of a desired value, as listed in the device
// configuration file,
#define VALUE_DESCR "25_Hz"


int
main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library.)
    blackstrap_init();

    // Load the device configuration
    zconfig_t *conf = blackstrap_i2cd_field_load("lsm303dlhc-accelerometer");
    if (!conf)
	blackstrap_log_error_exit("Unable to load conf");

    // Connect to i2cd
    zsock_t *sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
	blackstrap_log_error_exit("Unable to connect to i2cd");

    // Create a timespec structure to hold the timestamp
    struct timespec timestamp;

    // Fetch the value
    int value = blackstrap_i2cd_field_get(sock, conf, BUS, FIELD, &timestamp);
    if (value < 0)
	blackstrap_log_error_exit("Unable to connect to i2cd");

    blackstrap_log_info("At %s, the value of %s was 0x%02x",
	    format_time_8601plus(&timestamp), FIELD, value);

    // Fetch a string representing the value from the "descr" block
    // in the device configuration. For example, if the output_data_rate
    // field contains the value 4, this will return "50_Hz".
    char *descr = blackstrap_i2cd_field_str_get(sock, conf, BUS, FIELD,
	    &timestamp);
    if (!descr)
	blackstrap_log_error_exit("Unable to get value description "
		"for field %s", FIELD);

    blackstrap_log_info("At %s, the descriptive value of %s is %s\n",
	    format_time_8601plus(&timestamp), FIELD, descr);
    free(descr);

    // Set the field using the description from the descr block in the
    // device configuration. For example, setting the field to "25_Hz"
    // results in the field being set to the value 3. 
    if (blackstrap_i2cd_field_str_set(sock, conf, BUS, FIELD, VALUE_DESCR) < 0)
	blackstrap_log_error_exit("Unable to set field %s to %s",
		FIELD, VALUE_DESCR);

    // Destroy the socket
    zsock_destroy(&sock);

    // Unload the device configuration. This must be done explicitely,
    // since it was loaded specifically here.
    blackstrap_i2cd_field_unload(&conf);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

