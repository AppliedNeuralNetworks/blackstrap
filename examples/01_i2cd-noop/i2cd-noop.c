
#include <blackstrap.h>

// The listening port of running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

int main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library).
    blackstrap_init();

    // Create a ZMQ socket and connect to i2cd's listening port. i2cd
    // should be running and listening on the specified port. To change
    // i2cd's port bindings, edit i2cd.conf, and restart i2cd with
    // "/path/to/i2cd -r".
    zsock_t *sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
	blackstrap_log_error_exit("Unable to connect to i2cd");

    // Send a NOOP request to i2cd. 
    // This request does not interact with
    // the i2c bus on the destination host, but only returns a message
    // containing a status frame containing a successful (0)
    // status, and a timestamp.
    // Client side, i2cd_noop() handles receiving the message
    // and extracting and returning this status.  Note that ZMQ sockets
    // aren't like TCP sockets that fail immediately if the destination
    // is unroutable or isn't listening.  Instead, this operation will
    // block and wait for the server. To see this in action, try
    // starting i2cd AFTER you run this example.
    int result = blackstrap_i2cd_noop(sock, NULL);

    // Check the result. This will always be successful.
    blackstrap_log_info("Request %s\n", (result < 0) ? "failed" : "succeeded");

    // Destroy the socket.
    zsock_destroy(&sock);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(result);
}

