#!/usr/bin/env python3

import sys
from os.path import realpath, dirname, join

# Add project Python library to the import path
script_dir = dirname(sys.argv[0])
lib_dir = join(script_dir, '..', '..', 'pyblackstrap', 'lib')
sys.path.insert(0, realpath(lib_dir))

import blackstrap

# Create a ZMQ socket and connect to i2cd's listening port. i2cd
# should be running and listening on the specified port. To change
# i2cd's port bindings, edit i2cd.conf, and restart i2cd with
# "/path/to/i2cd -r".
i2cd = blackstrap.I2Cd('tcp://127.0.0.1:1255')

# Send a NOOP request to i2cd. This request does not interact with the i2c
# bus on the destination host, but only returns a message containing only
# a status frame containing a successful (0) status, and a timestamp
# Client side, noop() handles receiving the message and extracting the
# timestamp. Note that ZMQ sockets aren't like TCP sockets that fail
# immediately if the destination is unroutable or isn't listening.
# Instead, this operation will block and wait for the server. To see this
# in action, try starting i2cd AFTER you run this example.
timestamp = i2cd.noop()

# The timestamp is returned as a datetime object. If you need a starting
# time for a series of samples, you can use noop() to get the time without
# performing an action on the I2C bus.
print(timestamp)

