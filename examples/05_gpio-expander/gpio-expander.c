/*
 * This program will blink an LED connected to a pin of an MCP23017
 * at the specified rate. The assumption is made that the chip has
 * not yet been configured to run in "bank" mode. (If your chip has
 * been recently powered on or reset, and you haven't turned bank
 * mode on, this program will find the registers mapped to the normal
 * locations and should work properly.) Press Ctrl-C to stop blinking.
 */

#include <blackstrap.h>

// The listening port of running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

// The I2C bus on the destination host. For example, the Raspberry Pi
// Model B provides a connection to I2C bus 1 on the large header block.
#define BUS (1)

// The blink rate of the LED
#define BLINK_HZ (1)

// The desired output pin, a0-a7 or b0-b7. Connect the anode of an LED
// to this pin, and connect a suitable ballast resistor from the cathode
// to ground.
#define OUTPUT_PIN "a0"

void ensure_nonbank_mode(zsock_t *sock, zconfig_t *conf);

int
main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library.)
    blackstrap_init();

    // Load the device configuration
    zconfig_t *conf = blackstrap_i2cd_field_load("mcp23017-16-bit-mode");
    if (!conf)
	blackstrap_log_error_exit("Unable to load conf");

    // Connect to i2cd
    zsock_t *sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
	blackstrap_log_error_exit("Unable to connect to i2cd");

    // Be certain the chip is in non-banked (16-bit) mode
    ensure_nonbank_mode(sock, conf);

    // Calculate the sleep time, in nanoseconds
    long wait = (long)(500000000/BLINK_HZ);
    long wait_sec = wait / 1000000000;
    long wait_nsec = wait - wait_sec;
    struct timespec sleep_time = { wait_sec, wait_nsec };

    // Set the I/O direction 
    blackstrap_i2cd_field_str_set(sock, conf, BUS, 
	    "io_direction_" OUTPUT_PIN, "output");

    // Toggle the pin between high and low at the specified rate.
    do {
	blackstrap_i2cd_field_str_set(sock, conf, BUS,
		"gpio_" OUTPUT_PIN, "high");
	if(nanosleep(&sleep_time, NULL)) // That is, if Ctrl-C was pressed.
	    break;

	blackstrap_i2cd_field_str_set(sock, conf, BUS,
	       	"gpio_" OUTPUT_PIN, "low");
	if (nanosleep(&sleep_time, NULL))
	    break;
    } while (1);

    // Ensure the pin has been deactivated, and set it back to input state
    blackstrap_i2cd_field_str_set(sock, conf, BUS,
	    "gpio_" OUTPUT_PIN, "low");
    blackstrap_i2cd_field_str_set(sock, conf, BUS, 
	    "io_direction_" OUTPUT_PIN, "input");

    // Destroy the socket
    zsock_destroy(&sock);

    // Unload the device configuration. This must be done explicitely,
    // since it was loaded specifically here.
    blackstrap_i2cd_field_unload(&conf);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

void ensure_nonbank_mode(zsock_t *sock, zconfig_t *conf) {
    // It is possible that the MCP23017 is in banked (dual 8-bit) mode.
    // Because the register positions change, there's no single place
    // to read a value that indicates this mode. Meanwhile, IOCON has
    // a distinct characteristic: bit 0 will always read as 0, even
    // if a 1 has previously been written to it. Unfortunately, this
    // delicate probing will briefly activate interrupts for pin B0
    // if chip was in 16-bit mode and interrupts weren't enabled for
    // pin B0. Do not use this technique if this might disrupt operation.

    // In banked mode, 0x05 is a valid address for IOCON. This would be
    // GPINTENB in non-banked mode. Since bit 0 of IOCON would always
    // read as 0, then if bit 0 is set, then certainly the chip is in
    // non-banked mode and register 0x05 is indeed GPINTENB, and is
    // configured to enable interrupts on pin B0.
    if (blackstrap_i2cd_field_get(sock, conf, BUS,
	       	"gp_interrupt_enable_b0", NULL))
	return;

    // Meanwhile, if bit 0 is 0, 0x05 may still be GPINTENB in
    // banked mode. Test by setting bit 0 and reading it back.
    blackstrap_i2cd_field_str_set(sock, conf, BUS,
	    "gp_interrupt_enable_b0", "enable");


    // If bit 0 was settable, register 0x05 is GPINTENB in 16-bit
    // mode. Set it back to 0.
    if (blackstrap_i2cd_field_get(sock, conf, BUS,
		"gp_interrupt_enable_b0", NULL)) {
	blackstrap_i2cd_field_str_set(sock, conf, BUS,
		"gp_interrupt_enable_b0", "disable");
	return;
    }

    // Bit 0 wasn't settable, so 0x05 is certainly IOCON in
    // banked mode. Turn banked mode off.
    zconfig_t *bank_conf = blackstrap_i2cd_field_load("mcp23017-8-bit-mode");
    if (!conf)
	blackstrap_log_error_exit("Unable to load 8-bit conf");
    blackstrap_i2cd_field_str_set(sock, bank_conf, BUS,
	    "io_configuration_bank", "disable");
    blackstrap_i2cd_field_unload(&bank_conf);
}
