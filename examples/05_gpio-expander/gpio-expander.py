#!/usr/bin/env python3

# This program will blink an LED connected to a pin of an MCP23017
# at the specified rate. The assumption is made that the chip has
# not yet been configured to run in "bank" mode. (If your chip has
# been recently powered on or reset, and you haven't turned bank
# mode on, this program will find the registers mapped to the normal
# locations and should work properly.) Press Ctrl-C to stop blinking.

import sys
import time
from os.path import realpath, dirname, join

# Add project Python library to the import path
script_dir = dirname(sys.argv[0])
lib_dir = join(script_dir, '..', '..', 'pyblackstrap', 'lib')
sys.path.insert(0, realpath(lib_dir))

import blackstrap

# The listening port of running i2cd with which to make a connection.
endpoint = "tcp://127.0.0.1:1255"

# The I2C bus on the destination host. For example, the Raspberry Pi
# Model B provides a connection to I2C bus 1 on the large header block.
bus = 1

# The blink rate of the LED
blink_hz = 1

# The desired output pin, a0-a7 or b0-b7. Connect the anode of an LED
# to this pin, and connect a suitable ballast resistor from the cathode
# to ground.
output_pin = "a0"

# Create a ZMQ socket and connect to i2cd's listening port.
i2cd = blackstrap.I2Cd(endpoint)

# Load a field configuration file for this device
i2cd_field = blackstrap.I2CdField("mcp23017-16-bit-mode")

# Calculate the sleep time, in nanoseconds
sleep_time = 0.5/blink_hz

# Set the I/O direction 
i2cd_field.set_str(i2cd, bus, "io_direction_" + output_pin, 'output')

# Toggle the pin between high and low at the specified rate.
try:
    while True:
        i2cd_field.set_str(i2cd, bus, "gpio_" + output_pin, 'high')
        time.sleep(sleep_time)

        i2cd_field.set_str(i2cd, bus, "gpio_" + output_pin, 'low')
        time.sleep(sleep_time)
except KeyboardInterrupt:
    pass

# Ensure the pin has been deactivated, and set it back to input state
i2cd_field.set_str(i2cd, bus, "gpio_" + output_pin, 'low')
i2cd_field.set_str(i2cd, bus, "io_direction_" + output_pin, 'input')


