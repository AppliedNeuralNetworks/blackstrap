#!/usr/bin/env python3

import sys
from os.path import realpath, dirname, join

# Add project Python library to the import path
script_dir = dirname(sys.argv[0])
lib_dir = join(script_dir, '..', '..', 'pyblackstrap', 'lib')
sys.path.insert(0, realpath(lib_dir))

import blackstrap

# The I2C bus on the destination host. For example, the Raspberry Pi
# Model B provides a connection to I2C bus 1 on the large header block.
bus = 1

# The address of the device on the bus. This example uses the address
# of the L3GD20 Rate Gyro. Note that Linux's I2C interface takes the
# address shifted right. For clarity, the address is specified
# explicitly shifted.
address = 0xd6 >> 1

# Register 0x0f is often used for chip identification (WHO_AM_I). This
# example will retrieve and display the byte read from this location.
register = 0x0f

# Create a ZMQ socket and connect to i2cd's listening port.
i2cd = blackstrap.I2Cd('tcp://127.0.0.1:1255')

# Read the byte value from the chip
timestamp, value = i2cd.read_byte(bus, address, register)

# Display the timestamp and value
print("At time %s, the value of register 0x%02x was 0x%02x" %
      (timestamp, register, value))


