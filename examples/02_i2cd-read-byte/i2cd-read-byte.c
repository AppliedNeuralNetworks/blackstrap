
#include <blackstrap.h>

// The listening port of a running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

// The I2C bus on the destination host. For example, the Raspberry Pi
// Model B provides a connection to I2C bus 1 on the large header block.
#define BUS (1)

// The address of the device on the bus. This example uses the address
// of the L3GD20 Rate Gyro. Note that Linux's I2C interface takes the
// address shifted right. For clarity, the address is specified
// explicitly shifted.
#define ADDRESS  (0xd6 >> 1)

// Register 0x0f is often used for chip identification (WHO_AM_I). This
// example will retrieve and display the byte read from this location.
#define REGISTER (0x0f)

int main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library).
    blackstrap_init();

    // Create a ZMQ socket and connect to i2cd's listening port.
    zsock_t *sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
	blackstrap_log_error_exit("Unable to build socket");

    // Read the byte. If the timestamp is not needed, pass NULL instead of
    // a pointer to the timespec struct.
    struct timespec timestamp;
    int result = blackstrap_i2cd_read_byte(sock, BUS, ADDRESS, REGISTER,
	    &timestamp);
    if (result < 0)
	blackstrap_log_error_exit("Error reading byte value");

    // Display the time and value. This method of printing the timestamp
    // isn't very portable. See strftime() for a better, more portable
    // way to format time structures.
    blackstrap_log_info("At time %d.%ld, the value of register 0x%02x was "
	    "0x%02x\n", (int)timestamp.tv_sec, timestamp.tv_nsec, REGISTER,
	    result);

    // Destroy the socket
    zsock_destroy(&sock);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

