
#include <blackstrap.h>

// The listening port of running i2cd with which to make a connection.
#define ENDPOINT "tcp://127.0.0.1:1255"

// The I2C bus on the destination host. For example, the Raspberry Pi
// Model B provides a connection to I2C bus 1 on the large header block.
#define BUS 1


static zconfig_t *conf_accel;
static zconfig_t *conf_mag;
static zconfig_t *conf_gyro;
static zsock_t *sock;

void setup_9dof(void);
void setup_screen(void);
void show_accel(void);
void show_mag(void);
void show_gyro(void);
void box(int row, int color, const char *label, const char *units);
void field_print(int row, double value);
void cursor_pos(int row, int col);
void header_color(void);
void body_color(void);
void clear_color(void);


int main(void) {
    // Initialize Blackstrap: This is optional unless you need to load
    // configurations, get information about your application's path,
    // or if you wish to use the logging facility. (This will also
    // activate logging of errors within the Blackstrap library).
    blackstrap_init();

    // Load the device configurations
    conf_accel = blackstrap_i2cd_field_load("lsm303dlhc-accelerometer");
    if (!conf_accel)
        blackstrap_log_error_exit("Unable to load accelerometer conf");
    conf_mag = blackstrap_i2cd_field_load("lsm303dlhc-magnetometer");
    if (!conf_mag)
        blackstrap_log_error_exit("Unable to load magnetometer conf");
    conf_gyro = blackstrap_i2cd_field_load("l3gd20");
    if (!conf_mag)
        blackstrap_log_error_exit("Unable to load gyro conf");

    // Connect to i2cd
    sock = blackstrap_i2cd_connect(ENDPOINT);
    if (!sock)
        blackstrap_log_error_exit("Unable to connect to i2cd");

    setup_9dof();
    setup_screen();

    struct timespec sleep_time;
    sleep_time.tv_sec = 0;
    sleep_time.tv_nsec = 100000000L;

    do {
	show_accel();
	show_mag();
	show_gyro();
	printf("%c[22;1H\n", 27);
	fflush(stdout);
    } while (!nanosleep(&sleep_time, NULL));

    // Destroy the socket
    zsock_destroy(&sock);

    // Unload the device configurations. This must be done explicitely,
    // since each was loaded specifically here.
    blackstrap_i2cd_field_unload(&conf_accel);
    blackstrap_i2cd_field_unload(&conf_mag);
    blackstrap_i2cd_field_unload(&conf_gyro);

    // Gracefully clean up Blackstrap's application data structures. Among
    // other things, this closes logging and purges any loaded configurations.
    blackstrap_cleanup();

    exit(EXIT_SUCCESS);
}

void setup_9dof(void) {
    // Accelerometer is in power-down mode after reset. Set a data rate
    // to power it up and begin sampling. Set the full scale value as well.
    blackstrap_i2cd_field_str_set(sock, conf_accel, BUS,
	    "output_data_rate", "10_Hz");
    blackstrap_i2cd_field_str_set(sock, conf_accel, BUS,
	    "full_scale_value", "2_g");

    // Magnetometer needs only to be set to continuous conversion mode for
    // new values to appear.
    blackstrap_i2cd_field_str_set(sock, conf_mag, BUS,
	    "mode_select", "continuous_conversion");
    blackstrap_i2cd_field_str_set(sock, conf_mag, BUS,
	    "gain", "1.3_g");

    // Gyro needs to come out of power-down mode. Set full scale value.
    blackstrap_i2cd_field_str_set(sock, conf_gyro, BUS,
	    "full_scale_value", "250_dps");
    blackstrap_i2cd_field_str_set(sock, conf_gyro, BUS,
	    "power_down_mode", "normal_or_sleep_mode");
}

void setup_screen(void) {
    printf("%c[H%c[2J%c[47;30m %-79s%c[0m", 27, 27, 27,
	    "Blackstrap Example: Nice Degrees of Freedom", 27);
    box(3, 41, "Acceleration", "g");
    box(8, 42, "Magnetic Field", "gauss");
    box(13, 44, "Rotational Rate", "dps");
}

void show_accel(void) {
    int16_t *out_xyz = NULL;
    if (blackstrap_i2cd_field_get_range_int16(&out_xyz, sock, conf_accel, BUS,
	       	"out_xyz") < 0)
	blackstrap_log_error_exit("show_accel: unable to read range");
    for (int i = 0; i < 3; i++)
	field_print(i+4, -out_xyz[i] / 16384.);
    free(out_xyz);
}

void show_mag(void) {
    int16_t *out_xzy = NULL;
    if (blackstrap_i2cd_field_get_range_int16(&out_xzy, sock, conf_mag, BUS,
	       	"out_xzy") < 0)
	blackstrap_log_error_exit("show_mag: unable to read range");
    field_print(9, out_xzy[0] / 1100.);
    field_print(10, out_xzy[2] / 980.);
    field_print(11, out_xzy[1] / 980.);
    free(out_xzy);
}

void show_gyro(void) {
    int16_t *out_xyz = NULL;
    if (blackstrap_i2cd_field_get_range_int16(&out_xyz, sock, conf_gyro, BUS,
	       	"out_xyz") < 0)
	blackstrap_log_error_exit("show_gyro: unable to read range");
    for (int i = 0; i < 3; i++)
	field_print(i+14, out_xyz[i] / 16. / 2048. * 250.);
    free(out_xyz);
}

void box(int row, int color, const char *label, const char *units) {
    cursor_pos(row, 3);
    printf("%c[%d;37;1m %-20s", 27, color, label);
    body_color();
    for (int i = 0; i < 3; i++) {
	cursor_pos(row + i + 1, 3);
	printf(" %c:            %-6s", 'X' + i, units);
    }
    clear_color();
}

void field_print(int row, double value) {
    cursor_pos(row, 6);
    body_color();
    printf("%11.5f", value);
    clear_color();
}

void cursor_pos(int row, int col) {
    printf("%c[%d;%dH", 27, row, col);
}

void header_color(void) {
    printf("%c[44;37;1m", 27);
}

void body_color(void) {
    printf("%c[47;30m", 27);
}

void clear_color(void) {
    printf("%c[0m", 27);
}

