#!/usr/bin/env python3

import sys
from os.path import realpath, dirname, join
import time

# Add project Python library to the import path
script_dir = dirname(sys.argv[0])
lib_dir = join(script_dir, '..', '..', 'pyblackstrap', 'lib')
sys.path.insert(0, realpath(lib_dir))

import blackstrap

# The listening port of running i2cd with which to make a connection.
endpoint = "tcp://127.0.0.1:1255"

# The I2C bus on the destination host. For example, the Raspberry Pi
# Model B provides a connection to I2C bus 1 on the large header block.
bus = 1


def cursor_pos(row, col):
    print("\x1b[%d;%dH" % (row, col), end='')

def header_color():
    print("\x1b[44;37;1m", end='')

def body_color():
    print("\x1b[47;30m", end='')

def clear_color():
    print("\x1b[0m", end='')

def box(row, color, label, units):
    cursor_pos(row, 3)
    print("\x1b[%d;37;1m %-20s" % (color, label), end='')
    body_color()
    for i in range(3):
        cursor_pos(row + i + 1, 3)
        print(" %s:            %-6s" % (chr(ord('X') + i), units), end='')
    clear_color()

def setup_screen():
    print("\x1b[H\x1b[2J\x1b[47;30m %-79s\x1b[0m" % 
          "Blackstrap Example: Nice Degrees of Freedom", end='')
    box(3, 41, "Acceleration", "g")
    box(8, 42, "Magnetic Field", "gauss")
    box(13, 44, "Rotational Rate", "dps")

def field_print_float(row, value):
    cursor_pos(row, 6)
    body_color()
    print("%11.5f" % value, end='')
    clear_color()

def show_accel():
    timestamp, values = accel_field.get_range_int16(i2cd, bus, "out_xyz")
    for i in range(3):
        field_print_float(i+4, -values[i] / 16384.)

def show_mag():
    timestamp, values = mag_field.get_range_int16(i2cd, bus, "out_xzy")
    field_print_float(9, values[0] / 1100.);
    field_print_float(10, values[2] / 980.);
    field_print_float(11, values[1] / 980.);

def show_gyro():
    timestamp, values = gyro_field.get_range_int16(i2cd, bus, "out_xyz")
    for i in range(3):
        field_print_float(i+14, values[i] / 16. / 2048. * 250.)


# Create a ZMQ socket and connect to i2cd's listening port.
i2cd = blackstrap.I2Cd(endpoint)

# Load field configurations file each device
accel_field = blackstrap.I2CdField("lsm303dlhc-accelerometer")
mag_field = blackstrap.I2CdField("lsm303dlhc-magnetometer")
gyro_field = blackstrap.I2CdField("l3gd20")

# Accelerometer is in power-down mode after reset. Set a data rate
# to power it up and begin sampling. Set the full scale value as well.
accel_field.set_str(i2cd, bus, "output_data_rate", "10_Hz")
accel_field.set_str(i2cd, bus, "full_scale_value", "2_g")

# Magnetometer needs only to be set to continuous conversion mode for
# new values to appear.
mag_field.set_str(i2cd, bus, "mode_select", "continuous_conversion")
mag_field.set_str(i2cd, bus, "gain", "1.3_g")

# Bring gyro out of power down mode and set its full scale value
gyro_field.set_str(i2cd, bus, "full_scale_value", "250_dps")
gyro_field.set_str(i2cd, bus, "power_down_mode", "normal_or_sleep_mode")

setup_screen()
while True:
    show_accel()
    show_mag()
    show_gyro()
    print("\x1b[22;1H", end='')
    sys.stdout.flush()
    time.sleep(.1)

